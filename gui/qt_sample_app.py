import sys

from PyQt6.QtCore import QSize, Qt, QDate, QRegularExpression
from PyQt6.QtGui import QPixmap, QFont, QRegularExpressionValidator
from PyQt6.QtWidgets import (QApplication, QWidget, QLabel, QMessageBox,
                             QPushButton, QLineEdit, QCheckBox, QVBoxLayout, QHBoxLayout, QGridLayout,
                             QSpinBox, QComboBox, QDoubleSpinBox, QDateEdit, QDateTimeEdit, QTextEdit)


class MainWindow(QWidget):
    SIZE = QSize(400, 400)
    CAT_IMAGE = 'resources/cat.jpg'

    def __init__(self):
        super().__init__()
        self.init_ui()
        self.show()

    def init_ui(self):
        self.setWindowTitle('My Application')
        # self.resize(self.SIZE)
        self.setFixedSize(self.SIZE)

        main_layout = QVBoxLayout(self)

        self.hello_label = QLabel('Hello World!')
        self.hello_label.setFont(QFont('Arial', 18))
        self.hello_label.setWordWrap(False)
        self.hello_label.setAlignment(Qt.AlignmentFlag.AlignCenter)  # text alignment within widget
        main_layout.addWidget(self.hello_label)

        self.image_label = QLabel()
        self.image_label.setPixmap(QPixmap(MainWindow.CAT_IMAGE))
        self.image_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        main_layout.addWidget(self.image_label)

        self.button = QPushButton('Do it!')
        self.button.setEnabled(True)
        # signal: clicked, slot: on_click
        self.button.clicked.connect(self.on_button_clicked)
        main_layout.addWidget(self.button)

        h_layout = QHBoxLayout()

        self.name_edit = QLineEdit()
        self.name_edit.resize(210, 20)
        self.name_edit.setClearButtonEnabled(True)
        self.name_edit.setPlaceholderText('User Name')
        self.name_edit.textEdited.connect(self.on_text_edit)
        h_layout.addWidget(self.name_edit)

        self.phone_edit = QLineEdit()
        self.phone_edit.setInputMask("(999) 9999-9999;_")
        h_layout.addWidget(self.phone_edit)

        self.cb = QCheckBox('Opt')
        self.cb.toggle()  # start checked
        self.cb.toggled.connect(self.on_cb_toggle)
        h_layout.addWidget(self.cb)

        main_layout.addLayout(h_layout)
        h_layout = QHBoxLayout()

        self.info_dialog_button = QPushButton('Show Dialog')
        self.info_dialog_button.clicked.connect(self.on_info_dialog_button_clicked)
        h_layout.addWidget(self.info_dialog_button)

        self.question_dialog_button = QPushButton('Question Dialog')
        self.question_dialog_button.clicked.connect(self.on_question_dialog_button_clicked)
        h_layout.addWidget(self.question_dialog_button)

        self.show_another_window_button = QPushButton('Another Window')
        self.show_another_window_button.clicked.connect(self.on_show_another_window_button_clicked)
        h_layout.addWidget(self.show_another_window_button)

        main_layout.addLayout(h_layout)
        main_layout.setContentsMargins(10, 10, 10, 10)

        h_layout = QHBoxLayout()

        self.spinbox = QSpinBox()
        self.spinbox.setRange(0, 10000)
        self.spinbox.setPrefix('€')
        self.spinbox.valueChanged.connect(self.on_spinbox_value_changed)
        h_layout.addWidget(self.spinbox)

        self.combobox = QComboBox()
        self.combobox.addItems(['Item 1', 'Item 2', 'Item 3'])
        self.combobox.activated.connect(self.on_combobox_changed)
        h_layout.addWidget(self.combobox)

        main_layout.addLayout(h_layout)
        h_layout = QHBoxLayout()

        self.double_spinbox = QDoubleSpinBox()
        h_layout.addWidget(self.double_spinbox)

        self.dt_edit = QDateTimeEdit()
        self.dt_edit.setDisplayFormat('dd/MM/yyyy HH:mm')
        self.dt_edit.setMaximumDate(QDate().currentDate())
        self.dt_edit.setCalendarPopup(True)
        self.dt_edit.setDate(QDate(2020, 1, 1))
        h_layout.addWidget(self.dt_edit)

        main_layout.addLayout(h_layout)
        h_layout = QHBoxLayout()

        self.email_edit = QLineEdit()
        self.email_edit.setValidator(QRegularExpressionValidator(
            QRegularExpression(r'\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[com]{3}\b')))
        h_layout.addWidget(self.email_edit)

        self.validate_button = QPushButton('Validate')
        self.validate_button.clicked.connect(self.validate_email)
        h_layout.addWidget(self.validate_button)

        main_layout.addLayout(h_layout)

    def on_button_clicked(self):
        self.hello_label.setText(self.name_edit.text())
        self.name_edit.clear()

    def on_info_dialog_button_clicked(self):
        # first param is the parent widget
        QMessageBox.information(self, 'Modal Dialog', 'Modal dialog text', QMessageBox.StandardButton.Ok)

    def on_question_dialog_button_clicked(self):
        # first param is the parent widget
        answer = QMessageBox.question(self, 'Question',
                                      """
                                      <p>A simple question:</p>
                                      <p>Do you like sushi?</p>
                                      """, QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No)
        if answer == QMessageBox.StandardButton.Yes:
            QMessageBox.information(self, 'Modal Dialog', 'Well Done!', QMessageBox.StandardButton.Ok)
        elif answer == QMessageBox.StandardButton.No:
            QMessageBox.warning(self, 'Modal Dialog', 'Hey you!', QMessageBox.StandardButton.Close)

    def on_show_another_window_button_clicked(self):
        self.another = AnotherWindow()
        self.close()
        self.another.show()

    def on_cb_toggle(self, checked):
        sender = self.sender()  # event sender

        if checked:
            self.hello_label.setText(f'{sender.text()} Enabled!')
        else:
            self.hello_label.setText('Disabled!')

    def on_text_edit(self, text: str):
        pass

    def on_spinbox_value_changed(self, value):
        if value % 10 == 0:
            print(value)

    def validate_email(self):
        if self.email_edit.hasAcceptableInput():
            QMessageBox.information(self, 'Success', 'OK', QMessageBox.StandardButton.Ok)
        else:
            QMessageBox.critical(self, 'Error', 'Incorrect e-mail address.', QMessageBox.StandardButton.Discard)

    def on_combobox_changed(self, selected_index):
        print('Selected item index:', selected_index)


# noinspection PyUnresolvedReferences
class AnotherWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Another Window!')
        self.setFixedSize(200, 150)

        main_layout = QGridLayout(self)

        self.text_edit = QTextEdit()  # multi-line
        main_layout.addWidget(self.text_edit, 0, 0, 3, 1)  # takes 3 rows and 1 column

        main_layout.addWidget(QDateEdit(), 0, 1)  # add to row 0 col 1
        main_layout.addWidget(QCheckBox('Option 1'), 1, 1)  # add to row 1 col 1
        main_layout.addWidget(QCheckBox('Option 2'), 2, 1)  # add to row 2 col 1


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec())
