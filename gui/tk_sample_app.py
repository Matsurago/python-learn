import tkinter as ui
from tkinter.scrolledtext import ScrolledText


def create_window():
    window = ui.Tk()
    window.geometry("600x400")
    window.title("Sample Application")

    input_area = ScrolledText(window, width=50, height=10)
    input_area.insert("end-1c",
                      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9")
    input_area.pack()

    output_area = ScrolledText(window, width=50, height=10)
    output_area.pack()

    def on_decode_button_clicked():
        text = input_area.get("1.0", "end-1c")
        result = 'output: ' + text

        output_area.delete("1.0", "end-1c")
        output_area.insert("end-1c", result)

    decode_button = ui.Button(window, text="Decode", command=on_decode_button_clicked)
    decode_button.pack()

    return window


if __name__ == '__main__':
    main_window = create_window()
    main_window.mainloop()
