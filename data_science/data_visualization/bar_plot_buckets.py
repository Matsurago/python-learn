from collections import Counter
from typing import Dict

import matplotlib.pyplot as plt


def place_into_bucket(value: int) -> int:
    return min(value // 10 * 10, 90)


def make_histogram(buckets: Dict[int, int], num_buckets: int, bucket_size: int):
    plt.bar([x + (bucket_size // 2) for x in buckets.keys()],  # x-values (shifted by half a bucket size to center)
            buckets.values(),     # y-values
            bucket_size,          # set bar width
            edgecolor=(0, 0, 0))  # black edges for each bar

    min_x = -(bucket_size // 2)
    max_x = bucket_size * num_buckets + (bucket_size // 2)
    max_y = 1 + max(buckets.values())

    plt.axis([min_x, max_x, 0, max_y])   # x-axis and y-axis ranges
    plt.xticks([bucket_size * i for i in range(bucket_size + 1)])   # x-axis labels


grades = [83, 95, 91, 87, 70, 0, 85, 82, 100, 67, 73, 77, 0]
histogram = Counter(place_into_bucket(grade) for grade in grades)
make_histogram(histogram, 10, 10)

plt.xlabel("Decile")
plt.ylabel("# of Students")
plt.title("Distribution of Exam Grades")
plt.show()
