import matplotlib
import matplotlib.pyplot as plt
import numpy as np

input_file = './input/advection.txt'

# read data from the file produced by Fortran
with open(input_file, encoding = 'utf-8') as f:
    lines = f.readlines()

data = [
    [float(token)
     for line in lines[i:i+21]
     for token in line.split()]
for i in range(0, len(lines), 21)]
# end reading

matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 16})

time = [values[0] for values in data]
h = np.array([values[1:] for values in data])
x = np.arange(1, h.shape[1]+1)
time_steps = [0, 25, 50, 75]

fig = plt.figure(figsize=(8, 10))
axes = [plt.subplot2grid((4, 1), (row, 0), colspan=1, rowspan=1)
        for row in range(4)]

for ax in axes:
    n = axes.index(ax)
    ax.plot(x, h[time_steps[n], :], 'b-')
    ax.fill_between(x, 0, h[time_steps[n], :], color='b', alpha=0.4)
    ax.grid()
    ax.set_xlim(1, 100)
    ax.set_ylim(0, 1)
    ax.set_ylabel('Height', fontsize=16)
    ax.set_xticks([25, 50, 75, 100])
    ax.set_yticks([0, 0.25, 0.5, 0.75, 1])
    ax.set_title('Time step ' + '%3i' % time_steps[n])

for ax in axes[:-1]:
    ax.set_xticklabels([])

axes[3].set_xlabel('', fontsize=16)
axes[-1].set_xlabel('Spatial grid index')

plt.savefig('advection.svg')
plt.close(fig)
