import random

import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns


def next_random(min_value, max_value):
    return random.randrange(min_value, max_value + 1)


def update(frame_number, num_rolls_per_frame, faces, frequencies):
    """Configures each animation frame"""
    for i in range(num_rolls_per_frame):
        frequencies[next_random(1, 6) - 1] += 1

    total_num_rolls = sum(frequencies)

    plt.cla()  # clears plot
    axes = sns.barplot(x=faces, y=frequencies)  # creates a new plot

    axes.set_title(f'Frequencies for {total_num_rolls:,} Rolls')
    axes.set(xlabel='Face Value', ylabel='Frequency')

    axes.set_ylim(top=max(frequencies) * 1.1)  # scale y-axis by 10%

    # display frequency and percentage above each bar ('patch')
    for bar, frequency in zip(axes.patches, frequencies):
        axes.text(bar.get_x() + bar.get_width() / 2.0,   # text x-pos
                  bar.get_height(),                      # text y-pos
                  f'{frequency:,}\n{frequency / total_num_rolls:.3%}',  # text
                  ha='center',  # horizontal alignment
                  va='bottom')  # vertical alignment


sns.set_style('whitegrid')  # adds grey horizontal lines on the background
figure = plt.figure('Rolling Dice')   # window title

face_values = list(range(1, 7))
face_frequencies = [0] * 6

# need to store reference to animation
anim = animation.FuncAnimation(figure, update,
                               repeat=False,  # do not restart the animation when it ends
                               frames=2000,   # total number of frames
                               interval=33,   # ms between frames
                               fargs=(1, face_values, face_frequencies))
plt.show()
