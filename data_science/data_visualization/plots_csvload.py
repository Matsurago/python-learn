import matplotlib.pyplot as p
import pandas as pd


df = pd.read_csv("input/normal_dist.csv", header=None)
p.plot(df.iloc[:, 0], df.iloc[:, 1], linewidth=2)

p.title("N(50, 10)", fontsize=18)
p.xlabel("x", fontsize=14)
p.ylabel("f(x)", fontsize=14)
p.tick_params(axis="both", labelsize=14)
p.show()
