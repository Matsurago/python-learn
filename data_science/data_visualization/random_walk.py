from pathlib import Path

import pandas as pd
import matplotlib.pyplot as plt


rw_dist = pd.read_csv(Path('./input/random_walk_dist.csv'), names=['xs', 'ys'])
rw_pos = pd.read_csv(Path('./input/random_walk_pos.csv'), names=['xs', 'ys'])
biased_rw_dist = pd.read_csv(Path('./input/biased_random_walk_dist.csv'), names=['xs', 'ys'])
biased_rw_pos = pd.read_csv(Path('./input/biased_random_walk_pos.csv'), names=['xs', 'ys'])

sqrt_steps = [x ** 0.5 for x in rw_dist['xs']]
steps_005 = [x * 0.05 for x in rw_dist['xs']]

fig, (ax1, ax2) = plt.subplots(2, figsize=[6.4, 8.8])
plt.subplots_adjust(hspace=0.4)

ax1.set_title('Mean Distance from Origin (10 000 Trials)')
ax1.set_xlabel('number of steps')
ax1.set_ylabel('distance from origin')
ax1.plot(rw_dist['xs'], rw_dist['ys'], 'r-', label='random walk')
ax1.plot(rw_dist['xs'], sqrt_steps, 'o', label='square root of steps')

ax1.plot(biased_rw_dist['xs'], biased_rw_dist['ys'], 'g-', label='biased random walk')
ax1.plot(biased_rw_dist['xs'], steps_005, 'o', label='num steps * 0.05')
ax1.legend()

ax2.set_title('Final Positions (10 000 Steps)')
ax2.set_xlabel('steps west/east')
ax2.set_ylabel('steps north/south')
ax2.set_xlim(-800, 800)
ax2.set_ylim(-800, 800)

ax2.scatter(rw_pos['xs'][:100], rw_pos['ys'][:100], marker='+', c='k', label='random walk')
ax2.scatter(biased_rw_pos['xs'][:100], biased_rw_pos['ys'][:100], marker='^', c='r', label='biased random walk')
ax2.legend()

plt.show()
