import matplotlib.pyplot as plt


distance_from_center = [1, 3, 4, 7, 12]
average_property_price = [900, 850, 600, 700, 800]
labels = ['downtown', 'a', 'b', 'c', 'd']

plt.scatter(distance_from_center, average_property_price)

for dist, price, label in zip(distance_from_center, average_property_price, labels):
    plt.annotate(
        label,
        xy=(dist, price),   # point to label
        xytext=(5, -5),     # label offset
        textcoords='offset points')

plt.ylim(0, 1000)

plt.title('Property Price')
plt.xlabel('distance from center (km)')
plt.ylabel('price (thousands of USD)')
plt.show()
