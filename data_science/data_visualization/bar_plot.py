import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


def generate_numbers(count: int, min_value: int, max_value: int) -> np.ndarray:
    return np.random.randint(min_value, max_value + 1, count)


rolls = generate_numbers(min_value=1, max_value=6, count=1000)
values, frequencies = np.unique(rolls, return_counts=True)

sns.set_style('whitegrid')

axes = sns.barplot(x=values, y=frequencies, palette='bright')
axes.set_title('Dice roll simulation')
axes.set(xlabel='value', ylabel='frequency')

plt.show()
