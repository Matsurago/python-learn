import matplotlib.pyplot as plt


xs = [1, 2, 3, 4]
ys = [1, 2, 3, 4]
plt.plot(xs, ys, 'b-', label='first')

ys2 = [1, 7, 3, 5]
plt.plot(xs, ys2, 'r--', label='second')

plt.legend()
plt.show()
