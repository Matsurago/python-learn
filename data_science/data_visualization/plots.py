import matplotlib.pyplot as plt

xs = [0, 149.9, 361.9, 513.3, 895.1]
ys = [148, 127, 106, 95, 75]

plt.plot(xs, ys, linewidth=2, linestyle='solid', marker='o')
plt.title("Damage Reduction", fontsize=24)
plt.xlabel("armor", fontsize=14)
plt.ylabel("damage", fontsize=14)
plt.tick_params(axis="both", labelsize=14)
plt.xlim(-50, 1200)
plt.ylim(0, 160)

plt.annotate("  Zero", (0, 148))
plt.annotate("  Light", (361.9, 106))
plt.annotate("  Heavy", (895.1, 75))

plt.show()
