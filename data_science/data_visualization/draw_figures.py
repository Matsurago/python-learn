from collections import namedtuple

import matplotlib.pyplot as plt


Point = namedtuple("Point", ["x", "y"])


def draw_quadrilateral(a, b, c, d):
    plt.plot([p.x for p in (a, b, c, d, a)], [p.y for p in (a, b, c, d, a)])


def draw_fractal(a, b, c, d, depth=10, alpha=0.2):
    def middle_point(p1, p2):
        return Point(p1.x * (1 - alpha) + p2.x * alpha, p1.y * (1 - alpha) + p2.y * alpha)

    if depth < 1:
        return

    draw_quadrilateral(a, b, c, d)
    draw_fractal(middle_point(a, b), middle_point(b, c), middle_point(c, d), middle_point(d, a), depth - 1, alpha)


draw_fractal(Point(0, 0), Point(0, 1), Point(1, 1), Point(1, 0), 32, 0.1)
plt.show()
