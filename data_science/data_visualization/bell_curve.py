import matplotlib.pyplot as plt
from math import exp

x_center = 25
decay = 0.02

xs = range(100)
ys = [exp(-decay * (x - x_center) ** 2) for x in xs]

plt.plot(xs, ys, 'k-')
plt.show()
