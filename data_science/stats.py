import statistics

data = [1, 2, 1, 3, 3, 3, 4, 5, 10, 12]

print("mean:", statistics.mean(data))
print("median:", statistics.median(data))
print("mode:", statistics.mode(data))
print("standard deviation:", statistics.stdev(data))
