import numpy as np


# a 3-dim tensor can be considered as a stack of matrices
t = np.arange(27).reshape((3, 3, 3))
print(t, "\n----")

# replace 2nd matrix in a stack with matrix of ones
a = np.ones((3, 3))
t[1,:,:] = a
print(t, "\n----")

# same operation can be done with ellipsis '...' operator
t[0,...] = a
print(t)
