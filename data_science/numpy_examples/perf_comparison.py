import numpy as np
import random
import timeit


def random_1(n):
    return [random.randint(1, 7) for _ in range(n)]


def random_2(n):
    return np.random.randint(1, 7, n)


t = timeit.timeit('random_1(6_000_000)', globals=globals(), number=1)
print(f'{t:.2f} s')

t = timeit.timeit('random_2(6_000_000)', globals=globals(), number=1)
print(f'{t:.2f} s')
