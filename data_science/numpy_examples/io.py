import numpy as np


# load a single object from CSV file
a = np.loadtxt('data/sample.csv', delimiter=',')
print(a)

# save a single object as .npy file
np.save('data/sample.npy', a)

# load a single object from .npy file
b = np.load('data/sample.npy')
print(b)

# save a single object as CSV file
np.savetxt('data/sample_output.csv', b, delimiter=',')

# save/load multiple objects in .npz file
np.savez('data/samples.npz', a=a, b=b)
h = np.load('data/samples.npz')
# view stored array keys
print('stored array keys: ', list(h.keys()))
# get a stored array
c = h['a']
print(c)

# can use np.savez_compressed() for large, especially sparse, arrays
