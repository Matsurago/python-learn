import numpy as np


# create a two-dimensional array (a matrix)
matrix = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

# another way to create a 3x3 matrix
numbers = np.arange(9).reshape(3, 3)

# zero matrix (int)
zero_matrix = np.zeros((4, 4), dtype='int64')

# create an array filled with the specified element
m2 = np.full((2, 4), 7)   # 2x4 matrix

# 

# access (i, j)th element in a matrix
m12 = matrix[1, 2]
# select consecutive rows
rows12 = matrix[1:2]
# select non-consecutive rows
rows02 = matrix[[0, 2]]
# select the first column
col0 = matrix[:, 0]

# flatten a matrix (or a tensor) into a vector
assert list(matrix.flat) == [1, 2, 3, 4, 5, 6, 7, 8, 9]

# transpose
transposed_matrix = matrix.T  # a shallow copy

# get matrix dimensions
assert matrix.shape == (3, 3)
# get number of elements
assert matrix.size == 9
# get size (in bytes) of each element
assert matrix.itemsize == 4  # 32-bit int

# multiplying two matrices elementwise
a = np.arange(9).reshape((3, 3))
s = a * a
print(s, "\n----")

# multiplying row vectors in a matrix elementwise
v = np.arange(3)
s = v * a   # each row of a is multiplied by [0, 1, 2]
print(s, "\n----")

# multiplying matrices as in linear algebra
s = np.dot(a, a)
print(s)
