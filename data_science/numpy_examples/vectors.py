import numpy as np


# create an array (a vector)
numbers = np.array([1, 2, 4, 8])
print("type of elements:", numbers.dtype)
print("dimensions:", numbers.ndim)
print("shape:", numbers.shape)
print("total number of elements:", numbers.size)

# array of zeros
zeros = np.zeros(5)  # 1x5 zero vector (float)
# array of ones
ones = np.ones(5, dtype='int8')   # 1x5 vector of ones (byte size)

# create an array from a range
arr1 = np.arange(5, 10)      # [5, 6, 7, 8, 9]
arr1_reversed = arr1[::-1]   # [9, 8, 7, 6, 5]

# create a floating number array with step
arr2 = np.linspace(0.0, 1.0, num=5)  # [0, 0.25, 0.5, 0.75, 1]

# fast generation of random values
arr_rand = np.random.randint(1, 7, 1_000_000)  # a million dice rolls
arr_rand2 = np.random.random(1000)  # 1000 values in [0.0, 1.0)

# numpy operators (*, **, +=, etc.) work on the entire array
numbers_doubled = numbers * 2  # returns a new array: [2, 4, 8, 16]
numbers_inc = numbers + 1      # [2, 3, 5, 9]

# dot product
total_sales = np.dot([100, 50, 300], [2, 3, 1])
assert total_sales == 650
