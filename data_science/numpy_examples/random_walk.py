import numpy as np


def random_walk(n):
    steps = np.random.choice([-1, 1], n)
    return np.cumsum(steps)  # similar to itertools.accumulate()


print(random_walk(50))
