import numpy as np
from PIL import Image

image = Image.open('data/mountain.png')

# image to numpy array
im_data = np.array(image)
print(im_data.shape, im_data.dtype)  # tensor (3 channels: RGB)

# make the image gray
gray_image = image.convert('L')
gray_image.show()

g_data = np.array(gray_image)
print(g_data.shape, g_data.dtype)  # matrix (1 channel)
