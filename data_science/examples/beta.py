
def Beta(p, a, b):
    return (p ** (a-1)) * ((1-p) ** (b-1)) / beta(a, b)


def beta(a, b):
    return integrate(0, 1, lambda p: (p ** (a-1)) * ((1-p) ** (b-1)))


def integrate(x0, x1, f):
    step = (x1 - x0) / 1000
    res = 0
    
    x = x0
    while x <= x1:
        res += step * (f(x+step)+f(x)) / 2
        x += step

    return res


if __name__ == '__main__':
    res = integrate(0.45, 0.55, lambda p: Beta(p, 109, 111))
    print(res)
