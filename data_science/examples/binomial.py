def B(k, n, p):
    return C(n, k) * (p ** k) * ((1-p) ** (n-k))

def C(n, k):
    res = 1
    for i in range(n-k+1, n+1):
        res *= i
    
    return res / fact(k)

def fact(k):
    res = 1
    for i in range(2, k+1):
        res *= i
    return res

if __name__ == '__main__':
    res = 0
    for i in range(2, 26):
        res += B(i, 25, 0.1)

    print(res)

