import statistics


def std_dev(values):
    mean = sum(values) / len(values)
    dev_squared = [(value - mean) ** 2 for value in values]
    return (sum(dev_squared) / (len(values) - 1)) ** 0.5


grades = [98, 76, 71, 87, 83, 90, 57, 79, 82, 94]

print(std_dev(grades))
print(statistics.stdev(grades))
