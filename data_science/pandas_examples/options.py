import pandas as pd

# Set precision for floating point output
pd.set_option('display.precision', 2)

print(pd.Series([7.345, 3.1415926, 2.978]))
