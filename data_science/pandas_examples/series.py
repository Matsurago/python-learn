import pandas as pd

# Series is 1-dimensional array with optional labels for each value
grades = pd.Series([100, 97, 88], index=['Adam', 'Bob', 'Eva'])
print(grades)
print('----------------------')

# also can use a dictionary
grades2 = pd.Series({'John': 73, 'Max': 81, 'Anna': 99})
print(grades2)
print('----------------------')

# print various statistics (mean, standard deviance, max)
print(grades.describe())
print('----------------------')

# access a Series element by its label
print("Bob's grade:", grades['Bob'])  # or even simpler:
print("Eva's grade:", grades.Eva)
print('----------------------')

# string series: can use string manipulation methods
subjects = pd.Series(['Math', 'Literature', 'Chemistry'])
subjects_upper = subjects.str.upper()
print(subjects_upper)
print('----------------------')

# find whether all values match a regex (i.e. each value has three digits):
ids = pd.Series(['100', '110', '97'])
result = ids.str.match(r'\d{3}')  # returns a Series of booleans
print(result)
print('----------------------')

# find whether all values contain a regex:
ids = pd.Series(['AX999', 'RZ189', '5SUU1', '98X'])
result = ids.str.contains(r'[A-Z]{2}\d')
print(result)
print('----------------------')
