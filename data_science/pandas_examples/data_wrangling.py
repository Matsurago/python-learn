import numpy as np
import pandas as pd


# one way to deal with missing data is to replace it with average
values = pd.Series([98.6, 98.4, 98.7, 0.0])
cleaned_values = values.where(~np.isclose(values, 0.0, rtol=1e-3))  # 0.0 will be replaced with NaN,  ~ means "not"
cleaned_values.fillna(cleaned_values.mean(), inplace=True)  # replace NaN with mean
print(cleaned_values)

# use map to modify the data (here we omit the dash):
ids = pd.Series(['03-8899', '10-771', '15-999'])
result = ids.map(lambda val: val.replace('-', ''))
print(result)
