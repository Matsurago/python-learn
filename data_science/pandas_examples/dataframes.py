import pandas as pd

# DataFrames is 2-dimensional array with optional labels for rows and columns
grades = pd.DataFrame({
    'John': [75, 84, 91],
    'Susanna': [99, 88, 99],
    'Charles': [55, 72, 95]
}, index=['Math', 'Chemistry', 'Literature'])
print(grades)

# access a row in a DataFrame (using [] is not recommended)
print('-------- Math ----------')
print(grades.loc['Math'])  # use .iloc[x] to access by index (x = 0,1,...)

print('-------- Math & Chemistry ----------')
print(grades.loc['Math':'Chemistry'])  # using a slice here

print('-------- Math & Literature ----------')
print(grades.loc[['Math', 'Literature']])  # use a list to select specific rows
print('------------------------------------')

# access a column in a DataFrame
print('-------- Susanna ----------')
print(grades['Susanna'])

# access specific columns and specific rows
print('-------- Math & Literature (John, Charles) ----------')
print(grades.loc[['Math', 'Literature'], ['John', 'Charles']])
print('-----------------------------------------------------')

# access a cell in a DataFrame
print('John\'s Literature grade:', grades.at['Literature', 'John'])  # .iat() to access by indexes
print('------------------------------------')

# Boolean indexing (as in R)
# use & and | instead of 'and' and 'or'
print(grades[(grades > 80) & (grades < 95)])
print('------------------------------------')

# print descriptive statistics for a dataset (by column)
print(grades.describe())
print('------------------------------------')

# to describe by rows, transpose the table first
print(grades.T)

# sort by column name
# by default it returns a copy of data
print('------ Sorted by Column Name -------')
print(grades.sort_index(axis=1, ascending=True))  # use axis=0 to sort by row name

# sort by value (we transpose first to put students in rows and subjects in columns)
# by default it returns a copy of data
print('------ Sorted by Math Grade --------')
print(grades.T.sort_values(by='Math', ascending=False))  # use inplace=True to sort in place, saving memory
