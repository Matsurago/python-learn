import matplotlib.pyplot as plt
import pandas as pd


# can read CSV directly from the Internet
repo_url = 'https://vincentarelbundock.github.io/Rdatasets'
titanic_dataset_url = f'{repo_url}/csv/carData/TitanicSurvival.csv'
titanic_dataset = pd.read_csv(titanic_dataset_url)

pd.set_option('display.precision', 1)
titanic_dataset.columns = ['name', 'survived', 'sex', 'age', 'class']  # rename columns, if needed
print(titanic_dataset.head())

# statistics on survived passengers:
#   unique - num of distinct values (=2, True and False in our case)
#   top - the most frequently appearing value (False)
#   freq - num of rows with the top value (False, =809)
print((titanic_dataset.survived == 'yes').describe())

# build a histogram (on age, our single numeric value in the dataset)
hist = titanic_dataset.hist()
plt.show()
