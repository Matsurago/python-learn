from pathlib import Path
import pandas as pd


# read CSV (can read directly over HTTP/S)
data = pd.read_csv(Path('./data/accounts.csv'), names=['id', 'name', 'balance'])  # can supply headers
print(data)

# write CSV
data.to_csv(Path('./data/accounts_output.csv'), index=False)  # don't include row indexes
