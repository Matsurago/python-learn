# Additional Installation Requirements

## TextBlob

```
python -m textblob.download_corpora

import nltk
nltk.download('omw-1.4')
nltk.download('stopwords')

python -m spacy download en_core_web_sm
```

# Datasets

- [R Datasets (CSV)](https://vincentarelbundock.github.io/Rdatasets/datasets.html)
- [Awesome Public Datasets](https://github.com/awesomedata/awesome-public-datasets)
- [UCI ML Datasets](https://archive.ics.uci.edu/ml/datasets.php)

# Specific Datasets

- [Temperature Data](https://www.ncdc.noaa.gov/cag/city/time-series)

# Time Series Data

- [US Open Data Portal](https://data.gov/)
- [US Oceanic and Atmospheric Data](https://www.ncdc.noaa.gov/cag/)
- [NOAA Climate Time Series](https://www.esrl.noaa.gov/psd/data/timeseries/)
- [Quandl Financial Time Series](https://www.quandl.com/search)
- [Industry Time Series Data Library (TSDL)](https://datamarket.com/data/list/?q=provider:tsdl)
- [UCI Machine Learning Repository](http://archive.ics.uci.edu/ml/datasets.html)
- [University of Maryland - Economic Time Series](http://inforumweb.umd.edu/econdata/econdata.html)

# NLP Resources

- [Project Gutenberg (Books)](https://www.gutenberg.org)

# NLP Libraries
- [Stanford CoreNLP](https://stanfordnlp.github.io/CoreNLP/)
- [Apache OpenNLP](https://opennlp.apache.org/)

# Scikit-Learn CheatSheets
- [How to Choose an Estimator?](https://scikit-learn.org/stable/tutorial/machine_learning_map/index.html)
