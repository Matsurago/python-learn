from pathlib import Path

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


# New York City january temperatures
import scipy.stats

temps = pd.read_csv(Path('./data/NYC_jan_temp_1895-2022.csv'), skiprows=5,
                    names=['Date', 'Temperature', 'Anomaly'])

# remove month from dates: 189501 -> 1895
temps.Date = temps.Date.floordiv(100)

# linear regression
line = scipy.stats.linregress(x=temps.Date, y=temps.Temperature)
print(f'y = {line.slope:.2f}x + {line.intercept:.2f}')

# make prediction for year 2023
predicted_temp = line.slope * 2023 + line.intercept
print(f'Expected Jan 2023 temperature: {predicted_temp:.2f} F')

# make prediction for year 1880
predicted_temp = line.slope * 1890 + line.intercept
print(f'Predicted Jan 1890 temperature: {predicted_temp:.2f} F')

# plot the regression line with 95% confidence interval (shaded)
sns.set_style('whitegrid')
axes = sns.regplot(x=temps.Date, y=temps.Temperature)

# re-scale the Y-axis to empathize the linear relationship
axes.set_ylim(10, 70)

plt.show()
