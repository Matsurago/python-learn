import nltk.corpus
from textblob import TextBlob, Word


text = """The Helicon Club was at the end of the street. Women interested in literature, the arts, in social and public 
affairs could lunch, dine, entertain their friends in this oasis. Its pleasant rooms were large and cool, and, crowning 
boon in the very heart of modern Babylon, they offered even a measure of isolation. For the members' roll did not 
respond too readily to the length of "the waiting list."
"""
blob = TextBlob(text)

# get sentences
print('Sentences:', blob.sentences)
# get words
print('Words:', blob.words)
# Part-of-Speech (PoS) tagging
print('PoS tags:', blob.tags)
# extract noun phrases
print('Noun phrases:', blob.noun_phrases)
# extract N-grams
print('Trigrams: ', blob.ngrams(n=3))

# plural and singular forms (also works with WordList)
w = Word('corpora')
print(f'Singular of {w.string} is: {w.singularize()}')
w = Word('criterion')
print(f'Plural of {w.string} is: {w.pluralize()}')

# normalization
w = Word('indices')
print(f'Stemming: {w.string} -> {w.stem()}')
print(f'Lemmatization: {w.string} -> {w.lemmatize()}')  # requires additional corpus from NLTK

# WordNet features: definitions, synonyms, antonyms
w = Word('actualize')
print('[ Definitions ]')
print('\n'.join(w.definitions))
print('[ Synonyms ]')
print(w.synsets)  # in format <synonym>.<part-of-speech>.<meaning-index>

# NLTK stopwords
stop_words = nltk.corpus.stopwords.words('english')
filtered_text_words = [word for word in blob.words.lower() if word not in stop_words]
print('Stopword removal:', filtered_text_words)
