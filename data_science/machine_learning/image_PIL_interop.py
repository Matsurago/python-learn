from sklearn.datasets import load_sample_images
from PIL import Image

im = load_sample_images()

china_data = im.images[0]
print(china_data.shape, china_data.dtype)

flower_data = im.images[1]
print(flower_data.shape, flower_data.dtype)

china_image = Image.fromarray(china_data)
china_image.save('china.png')

image = Image.open('china.png')
image.show()
