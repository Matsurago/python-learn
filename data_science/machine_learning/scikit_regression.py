from pathlib import Path
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns


# load temperature dataset
temps = pd.read_csv(Path('../scipy_examples/data/NYC_jan_temp_1895-2022.csv'), skiprows=5,
                    names=['Date', 'Temperature', 'Anomaly'])

# clean the dates: 189501 -> 1895
temps.Date = temps.Date.floordiv(100)

# split data into 75% train data and 25% test data
x_train, x_test, y_train, y_test = train_test_split(
    temps.Date.values.reshape(-1, 1),  # reshape into N rows (inferred thanks to -1) and 1 column (2-dimensional)
    temps.Temperature.values,
    random_state=11
)
print(x_train.shape, x_test.shape)

lin_reg = LinearRegression()
lin_reg.fit(X=x_train, y=y_train)

# regression line: y = kx+b
k = lin_reg.coef_[0]
b = lin_reg.intercept_
print(f'y = {k:.2f}x{+b:.2f}')

# test the model
predicted = lin_reg.predict(x_test)

for p, e in zip(predicted, y_test):
    print(f'predicted = {p:.2f}, expected = {e:.2f}')

print(f'Predicted (2022): {(k * 2022 + b):.2f}')
print(f'Predicted (1880): {(k * 1880 + b):.2f}')

# visualize
axes = sns.scatterplot(data=temps, x='Date', y='Temperature', hue='Temperature',
                       palette='winter', legend=False)
axes.set_ylim(10, 70)

# regression line start and end points
x = np.array([min(temps.Date.values), max(temps.Date.values)])
y = lin_reg.coef_ * x + lin_reg.intercept_
plt.plot(x, y)

plt.show()
