from sklearn.datasets import load_digits
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.model_selection import KFold, cross_val_score, train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


digits = load_digits()  # digits dataset
print(digits.DESCR)  # dataset description (#features, #instances)

print(digits.data)  # instances
print(digits.data.shape)  # (1797, 64): 1797 instances, each of 64 features
print(digits.target)  # labels for instances (i.e. digits the images represent, 0..9)
print(digits.target.shape)  # (1797, ): matches the number of instances, all data is labelled

print(digits.images[7])  # image data for instance #7, 8x8 pixels from 0..16 shades of gray

# display first 24 images of digits
figure, axes = plt.subplots(nrows=4, ncols=4, figsize=(6, 4))
for ax, image, label in zip(axes.ravel(), digits.images, digits.target):
    ax.imshow(image, cmap=plt.cm.gray_r)
    ax.set_xticks([])  # remove x axis ticks
    ax.set_yticks([])  # remove y axis ticks
    ax.set_title(label)

plt.tight_layout()  # removes extra padding so plots can take more space
plt.show()

# randomly shuffles data, and splits it between train data and test data
# IMPORTANT: we assume samples are split evenly between classes
train_sample, test_sample, train_labels, test_labels = train_test_split(
    digits.data,
    digits.target,
    random_state=11  # seed for reproducibility
)

# by default, 75% of samples are set for training, and 25% for testing
print('Train data shape:', train_sample.shape)
print('Test data shape:', test_sample.shape)

# k-nearest neighbours estimator
knn = KNeighborsClassifier(n_neighbors=5)
# train the estimator (in kNN case: it is lazy, so no training)
knn.fit(X=train_sample, y=train_labels)

predicted_labels = knn.predict(X=test_sample)

# prediction with index 18 doesn't match
print('predicted:', predicted_labels[:20])
print('expected:', test_labels[:20])

# all incorrect predictions
incorrect_predictions = [(p, e) for p, e in zip(predicted_labels, test_labels) if p != e]
print('wrong prediction/expected:', incorrect_predictions)
incorrect_proportion = len(incorrect_predictions) / len(predicted_labels)
print(f'incorrect predictions: {incorrect_proportion:.2%}')

# get estimator accuracy
accuracy = knn.score(test_sample, test_labels)
print(f'accuracy: {accuracy:.2%}')

# print confusion matrix
confusion = confusion_matrix(y_true=test_labels, y_pred=predicted_labels)
print(confusion)

# print classification report
names = [str(digit) for digit in digits.target_names]
report = classification_report(test_labels, predicted_labels, target_names=names)
print(report)
# precision: true_positives / (true_positives + false_positives)
#     45 of '7' were correctly classified as '7' and one '3' and one '4' were classified as '7'
#     => precision('7') = 45 / 47 = 0.96
# recall: true_positives / (true_positives + true_negatives)
#     all 45 of '7' were correctly classified as '7'
#     => recall('7') = 1.00
#     39 of '8' were correctly classified as '8', and five of '8' were classified as other digits
#     => recall('8') = 39 / 44 = 0.89

# visualize the heat map
confusion_df = pd.DataFrame(confusion, index=range(10), columns=range(10))
sns.heatmap(confusion_df, annot=True, cmap='nipy_spectral_r')
plt.show()

# k-fold cross validation
# (selecting different subsets of train and test data from the dataset K times)
k_fold = KFold(
    n_splits=10,  # number of folds
    random_state=11,  # seed for reproducibility
    shuffle=True  # need to shuffle an ordered dataset to prevent train or test data to be all the same class
)

# calculate accuracy for each fold (n = 10 in our case)
scores = cross_val_score(
    estimator=knn,  # k-Nearest Neighbours estimator we created at beginning of this script
    X=digits.data,  # samples to use for training and testing
    y=digits.target,  # labels for samples
    cv=k_fold  # cross validation config
)
print(scores)
print(f'Mean accuracy: {scores.mean():.2%}')
print(f'Accuracy standard deviation: {scores.std():.2%}')

# let's compare kNN to other estimators: SVC and GaussianNB
estimators = {
    'knn': knn,
    'svc': SVC(),
    'gaussian': GaussianNB(),
}

for est_name, est in estimators.items():
    scores = cross_val_score(est, X=digits.data, y=digits.target, cv=k_fold)
    print(f'{est_name:>10}: mean acc = {scores.mean():.2%}, stddev = {scores.std():.2%}')
