import timeit


def perf(code: str, scope: dict, setup: str = ''):
    trials = timeit.repeat(setup=setup, stmt=code, globals=scope, repeat=10, number=1)
    #min_time = min(trials)
    for min_time in trials:
        if min_time >= 1:
            units = 's'
        elif min_time >= 1e-3:
            min_time *= 1e+3
            units = 'ms'
        elif min_time >= 1e-6:
            min_time *= 1e+6
            units = 'μs'
        else:
            min_time *= 1e+9
            units = 'ns'

        print(f'{min_time:.2f} {units}', end=', ')
    print()
    print()
