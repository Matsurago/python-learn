from .bitvector import BitVector


def test_empty():
    v = BitVector(7)
    assert all(item == 0 for item in v)
    assert v.storage == 0b10000000


def test_zero_length():
    v = BitVector()
    assert len(v) == 0


def test_set_values():
    v = BitVector(4)
    v.set(1)
    v.set(3)

    assert list(v) == [0, 1, 0, 1]


def test_set_all():
    v = BitVector(5)
    v.set_all()

    assert list(v) == [1, 1, 1, 1, 1]
    assert v.storage == 0b111111


def test_unset_values():
    v = BitVector(6)
    v.set_all()
    v.unset(4)
    v.unset(0)

    assert list(v) == [0, 1, 1, 1, 0, 1]


def test_extend():
    v = BitVector(6)
    v.set_all()
    v.unset(4)
    v.unset(0)

    assert v.storage == 0b1101110
    assert list(v) == [0, 1, 1, 1, 0, 1]

    u = v.copy_append_zero()
    assert u.storage == 0b10101110
    assert list(u) == [0, 1, 1, 1, 0, 1, 0]

    w = v.copy_append_one()
    assert w.storage == 0b11101110
    assert list(w) == [0, 1, 1, 1, 0, 1, 1]
