import random
from math import floor, log2


class Heap:
    def __init__(self, max_size, storage=None):
        self.size = max_size

        if storage is None:
            self.array = [0] * max_size
            self.n = 0
        else:
            if len(storage) < max_size:
                raise ValueError(f'Storage size {len(storage)} is less than the heap size {max_size}.')
            self.array = storage
            self.n = max_size

    @classmethod
    def from_list(cls, values):
        """
        Builds a heap from unsorted list.
        Time Complexity: O(n)
        """
        n = len(values)
        result = cls(n, values)

        for i in range(n // 2, -1, -1):  # leaves are good
            result.heapify(i)

        return result

    def heapify(self, i):
        """
        Fixes a single violation of the heap property at index i

        Precondition: left and right subtrees of i are proper heaps
        Time Complexity: O(log n)
        """

        right_pos = 2*i + 1
        left_pos = 2*i + 2
        max_child_pos = None

        if left_pos < self.n:
            max_child_pos = left_pos if self.array[right_pos] <= self.array[left_pos] else right_pos
        elif right_pos < self.n:
            max_child_pos = right_pos

        if max_child_pos and self.array[i] < self.array[max_child_pos]:
            self.array[i], self.array[max_child_pos] = self.array[max_child_pos], self.array[i]  # swap
            self.heapify(max_child_pos)  # ensure that the heap property in a subtree retains

    def __getitem__(self, i):
        return self.array[i]

    def __setitem__(self, i, value):
        self.array[i] = value

    def __str__(self):
        result = ''

        n = self.n
        height = floor(log2(n))
        spaces = 3 * (2 ** height)

        k = 0
        level = 0
        while k < n:
            i = 0
            s = ''
            while k < n and i < 2 ** level:
                s += f'{self.array[k]:^{spaces}}'
                k += 1
                i += 1

            result += s + '\n'
            level += 1
            spaces //= 2

        return result[:-1]


if __name__ == '__main__':
    heap = Heap.from_list([8, 2, 1, 4, 7, 3, 9, 0, 5])
    print(heap)

    random_list = [random.randint(1, 99) for i in range(30)]
    print(Heap.from_list(random_list))
