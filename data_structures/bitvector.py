from collections.abc import Sequence

__all__ = ['BitVector']


class BitVector(Sequence):
    def __init__(self, size: int = 0):
        self.size = size
        self.storage = 1 << self.size

    def __getitem__(self, i: int) -> int:
        if i < self.size:
            return (self.storage >> i) & 1
        raise IndexError(f'Index {i} is out of range')

    def set(self, i: int):
        self.storage |= (1 << i)

    def set_all(self):
        self.storage = (1 << (self.size + 1)) - 1

    def unset(self, i: int):
        self.storage &= ~(1 << i)

    def copy_append_zero(self):
        result = BitVector(self.size + 1)
        result.storage |= self.storage
        result.storage &= ~(1 << self.size)
        return result

    def copy_append_one(self):
        result = BitVector(self.size + 1)
        result.storage |= self.storage
        return result

    def __len__(self) -> int:
        return self.size

    def __str__(self):
        return str(list(self))


