import pytest


def add(a, b):
    return a + b


def test_add_float():
    assert add(2.0, 3.0) == pytest.approx(5.0)


def test_exception():  # failing test on purpose
    with pytest.raises(ValueError):
        add(5, 6)


def test_assert_in():
    assert add(2, 3) in [1, 3, 5]
    assert add(2, 3) not in [9, 12]
