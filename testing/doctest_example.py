def add(a, b):
    """
    Adds two numbers.

    >>> add(2, 3)
    5
    >>> add(3, 4)
    8
    """
    return a + b


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=False)
