import unittest


def add(x, y):
    return x + y


def setUpModule():
    print("< Before all tests, set expensive dependencies here >")

def tearDownModule():
    print("< After all tests, free resources here >")


class TestAdd(unittest.TestCase):
    def setUp(self):
        self.four = 4  # executed before each test

    def tearDown(self):
        pass  # executed after each test

    def test_add_zero(self):   # failing test on purpose
        self.assertEqual(add(2, 3), self.four)

    def test_exception(self):  # failing test on purpose
        with self.assertRaises(ValueError):
            add(5, 6)

    def test_add_something(self):
        self.assertNotEqual(add(2, 1), 2)

    def test_add_self(self):
        self.assertTrue(add(2, 2) == 4)
        self.assertFalse(add(1, 1) == 1)

    def test_assert_in(self):
        self.assertIn(add(2, 3), [1, 3, 5])
        self.assertNotIn(add(2, 3), [9, 12])


if __name__ == '__main__':
    unittest.main()
