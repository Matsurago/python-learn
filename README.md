# Python Code Snippets

## Installing Requirements
```
python -m venv env
./env/bin/activate
pip install -r requirements.txt
```

## Copying Environment
A virtual environment cannot be copied, because it relies
on hardcoded absolute paths. To recreate existing environment,
save the list of installed modules with:
```
python -m pip freeze > requirements.txt
```
Then create a new virtual environment and reinstall the packages
as described in the previous section.

## Python Tips
Start a local documentation server on the given port:
```
python -m pydoc -p 1234
```

To start a simple HTTP server that serves files:
```
python -m http.server
```

To add a location to search for Python modules, set the `PYTHONPATH` variable:
```
PYTHONPATH=/some/path python app.py
```

The proper structure for a mini Python app is
```
app/
  __init__.py
  __main__.py
  source.py
```
Then it can be run with `python app`.

## Useful Resources
Videos from Python conferences:
- [PyVideo - Many Python Videos from Conferences](https://pyvideo.org)
- [PyCon 2015](https://www.youtube.com/c/PyCon2015/videos?view=0&sort=p&flow=grid)
- [PyCon 2016](https://www.youtube.com/c/PyCon2016/videos?view=0&sort=p&flow=grid)
- [PyCon 2017](https://www.youtube.com/c/PyCon2017/videos?view=0&sort=p&flow=grid)
- [PyCon 2018](https://www.youtube.com/c/pycon2018/videos?view=0&sort=p&flow=grid)
- [PyCon 2019](https://www.youtube.com/c/pycon2019/videos?view=0&sort=p&flow=grid)
- [PyCon US 2020/2021](https://www.youtube.com/c/PyConUS/playlists)
- [Raymond Hettinger Talks](https://www.youtube.com/results?search_query=Raymond+Hettinger)
