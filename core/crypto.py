import hashlib


# sha256
h = hashlib.new('sha256')
h.update(b'hello world')

print('digest len:', h.digest_size)
print('digest:', h.digest())
print('hex digest:', h.hexdigest())
