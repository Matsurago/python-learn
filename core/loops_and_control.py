languages = ["English", "Russian", "Korean", "German", "Japanese", "French"]

for language in languages:
    print(": " + language, end=' ')
print()

# iterate backwards
for language in reversed(languages):
    print(": " + language, end=' ')
print()

# iterate on a sublist
for language in languages[1:3]:   # items 1, 2
    print(language)

# enumerate() wraps any iterator with a lazy generator
for i, language in enumerate(languages):
    print(f"index = {i}, item = {language}")

for i in range(1, 5):   # [1,4]
    print(i)

while False:
    pass  # do nothing

# null check
x = None
if x:
    print("x exists.")

# ternary operator (order is different than in other languages)
result = [] if x is None else x  # outputs x or an empty list
print(result)

# can check that a value is within a range more conveniently in conditions:
val = 48
if 18 < val < 99:
    print("within range")

kind = "cat"
if kind in ("mouse", "cat", "dog"):
    print("a pet")
