import timeit


def sum_1(n):
    return sum(range(n))


def sum_2(n):
    result = 0
    for i in range(n):
        result += i
    return result


max_n = 100_000
times = 100

# use timeit to compare performance of short pieces of code
exec_time = timeit.timeit('sum_1(max_n)', number=times, globals=globals())
print(f'[sum_1] Total: {exec_time}, per call: {exec_time / times}')

exec_time = timeit.timeit('sum_2(max_n)', number=times, globals=globals())
print(f'[sum_2] Total: {exec_time}, per call: {exec_time / times}')

# can also store stats for each trial
trials = timeit.repeat(
    setup='n = 100_000',   # init data
    stmt='sum_1(n)',       # code to benchmark
    globals=locals(),      # to which variables the code has access
    repeat=1000,           # number of trials
    number=1)              # number of executions of the code

# min is more useful than average, as longer executions are probably due to some interference
min_exec_time = min(trials)
print(f'[sum_1] Min execution time: {min_exec_time}, average: {sum(trials) / len(trials)}')
