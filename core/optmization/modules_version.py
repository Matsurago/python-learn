import sys
from typing import Dict

# list all currently loaded modules
loaded_modules: Dict = sys.modules
loaded_module_names = list(loaded_modules.keys())
print(f"Currently {len(loaded_module_names)} modules are loaded.")
print("Modules:", loaded_module_names)

# list of paths where to look for modules
# !! this list can be modified at runtime: sys.path.append("/my/path")
print("Path:", sys.path)

# show current python version at runtime
print(sys.version_info)
print(sys.version)
