import sys

# know how much memory an object occupies, in bytes
s1 = 'a white cat'
print(sys.getsizeof(s1), 'bytes')

my_list = list(range(100))
print(sys.getsizeof(my_list), 'bytes')

small_int = 127
print(f'{small_int}:', sys.getsizeof(small_int), 'bytes')
larger_int = 2_000_000_000
print(f'{larger_int}:', sys.getsizeof(larger_int), 'bytes')
huge_int = 2 ** 1000
print(f'{huge_int:e}:', sys.getsizeof(huge_int), 'bytes')

# a list contains only references, so that the size of lists of equal lengths are the same
words = ['abc', 'cde']
sentences = ['a long sentence here', 'an even longer sentence from a novel']
assert sys.getsizeof(words) == sys.getsizeof(sentences)
