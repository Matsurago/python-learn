class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age


person = Person('Olga', 27)
print(person)  # outputs <__main__.Person object at 0x00000255C60354B0>
print(person.__dict__)   # proper instance attribute output

x = 5
y = '5'
print(f"x={x}, y={y}")       # outputs 5 in both cases
print(f"x={x!r}, y={y!r}")   # properly outputs 5 and '5'
print(f"{x=}, {y=}")         # also properly outputs 5 and '5'
