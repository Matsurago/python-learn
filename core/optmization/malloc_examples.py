# helpful to find memory leaks
import os
import tracemalloc


def make_list():
    return [os.urandom(128) for _ in range(100)]


tracemalloc.start(10)   # 10 = stack depth
initial_snapshot = tracemalloc.take_snapshot()

values = make_list()

snapshot = tracemalloc.take_snapshot()

# stats are sorted from the biggest allocation to the smallest
stats = snapshot.compare_to(initial_snapshot, 'lineno')
for stat in stats:
    print(stat)
