import cProfile


def foo(n):
    s = ''
    for i in range(n):
        numbers = list(range(n))
        s += '.'.join([str(num) for num in numbers])
    return s


# use cProfile to run find which function is a performance bottleneck
# cProfile outputs statistics for time spend in each function in callstack
cProfile.run('foo(1000)')

# How to read the output:
# ncalls: number of calls made to the function
# tottime: total time spent in the function, excluding time in subfunctions
# percall: total time divided by the number of calls
# cumtime: cumulative time spent in the function and all subfunctions
# percall: cumulative time divided by the number of calls
# filename:lineno(function): the file the function is in and at which line number
