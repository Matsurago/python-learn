import bisect
from typing import List


def test_list_basics():
    languages: List[str] = ['C', 'C++', 'Ada']
    
    assert languages[-1] == 'Ada'  # get last element
    assert len(languages) == 3     # size of the list

    assert 'C++' in languages      # check whether an item exists in the list
    assert 'D++' not in languages  # check whether an item does not exist in the list

    assert str(languages) == "['C', 'C++', 'Ada']"  # list to string

    if languages:   # can convert to boolean (empty list = False, non-empty = True)
        assert True   


def test_list_constructors():
    digits = list(range(0, 10))  # from a range (right-hand limit is not included)
    assert digits == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    even_digits = list(range(1, 10, 2))
    assert even_digits == [1, 3, 5, 7, 9]

    letters = list('cat')  # creates a list of letters from a string
    assert letters == ['c', 'a', 't']

    zeroes = [0] * 5  # create list of length n (shallow copy!)
    assert zeroes == [0, 0, 0, 0, 0]


def test_splatting():
    values = [1, 2, 3]
    expanded = [7, *values, 8]
    assert expanded == [7, 1, 2, 3, 8]


def test_add():
    languages = ['C', 'C++']
    assert len(languages) == 2
    
    languages.append('Ada')         # add
    assert languages[2] == 'Ada'
    assert len(languages) == 3
    
    languages.insert(1, 'Clojure')  # add at specified index
    assert languages[1] == 'Clojure'
    assert len(languages) == 4


def test_delete():
    languages = ['C', 'C++', 'Haskell', 'Python', 'Java']
    assert len(languages) == 5

    last_item = languages.pop()     # remove and return the last element
    assert last_item == 'Java'
    assert languages == ['C', 'C++', 'Haskell', 'Python']

    second_lang = languages.pop(2)  # remove and return the element at specified index
    assert second_lang == 'Haskell'
    assert languages == ['C', 'C++', 'Python']

    del languages[0]                # remove by index
    assert languages == ['C++', 'Python']

    languages.remove('Python')      # remove by value; only the first occurrence is removed
    assert not ('Python' in languages)
    assert languages == ['C++']

    # remove list subrange at once
    years = [1990, 1991, 1992, 1993, 1994]
    del years[:3]
    assert years == [1993, 1994]


def test_slices():
    numbers = [1, 2, 3, 4, 5]

    new_numbers = numbers[:]         # copy the list (deep copy)
    new_numbers[0] = 4
    assert numbers[0] == 1
    assert new_numbers[0] == 4

    except_last = numbers[:-1]       # copy the list except the last element
    assert except_last == [1, 2, 3, 4]

    tail = numbers[-3:]              # copy last N items in the list
    assert tail == [3, 4, 5]         # N = 3 -> [3, 4, 5]

    first_digits = numbers[:2]       # the right index (assumed zero) is not included
    assert first_digits == [1, 2]

    many_digits = numbers[:50]       # can specify more elements that actually are in the list
    assert len(many_digits) == len(numbers)

    # can store slice indexes in an object to avoid magic numbers
    first_two = slice(0, 2)
    assert numbers[first_two] == [1, 2]


def test_slices_assign():
    numbers = [1, 2, 3, 4, 5]
    numbers[1:3] = [90, 91, 92]      # can insert another list into the current list
    assert numbers == [1, 90, 91, 92, 4, 5]


def test_star_operator():
    numbers = [1, 2, 3, 4, 5]

    # can select the remaining elements with star
    first, second, *last = numbers
    assert first == 1
    assert second == 2
    assert last == [3, 4, 5]

    # can be used in other positions
    least, *rest, largest = numbers
    assert least == 1
    assert rest == [2, 3, 4]
    assert largest == 5


def test_list_comprehensions():
    cubes = [n ** 3 for n in range(5)]

    assert cubes == [0, 1, 8, 27, 64]


def test_sort():
    numbers = [6, 4, 7, 2, 3, 1, 5]

    numbers.sort()
    assert numbers == [1, 2, 3, 4, 5, 6, 7]

    numbers.sort(reverse=True)
    assert numbers == [7, 6, 5, 4, 3, 2, 1]

    s_numbers = sorted(numbers)   # sort without mutating the original list
    assert numbers == [7, 6, 5, 4, 3, 2, 1]
    assert s_numbers == [1, 2, 3, 4, 5, 6, 7]

    # when sorting strings, capitalized words come first (ASCII order)
    words = ['A', 'a', 'Z', 'z']
    assert sorted(words) == ['A', 'Z', 'a', 'z']

    # to sort alphabetically, pass a sorting function
    assert sorted(words, key=str.lower) == ['A', 'a', 'Z', 'z']


def test_sort_objects():
    class Person:
        def __init__(self, name, age):
            self.name = name
            self.age = age

    persons = [Person('Ivan', 44), Person('Andrey', 62)]

    persons.sort(key=lambda person: person.name)
    assert persons[0].name == 'Andrey'

    persons.sort(key=lambda person: person.age)
    assert persons[0].name == 'Ivan'


def test_binary_search():
    # requires a sorted list
    capitals = ['Athens', 'Minsk', 'Moscow', 'Ottawa', 'Paris', 'Tokyo', 'Warsaw', 'Washington']
    assert 2 == bisect.bisect_left(capitals, 'Moscow')  # match

    # if there is no match, bisect_left() returns the position where the item should be inserted
    assert 1 == bisect.bisect_left(capitals, 'Brussels')

    # we know that the item is found if:
    #     a) the returned index < len(values)
    # and b) values[returned_index] == target


def test_list_api():
    letters = ['A', 'B', 'C']

    letters.reverse()  # reverse, mutates the list
    assert letters == ['C', 'B', 'A']


def test_functions_on_lists():
    numbers = [7, 12, 13]
    assert min(numbers) == 7
    assert max(numbers) == 13
    assert sum(numbers) == 32
