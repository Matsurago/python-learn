from collections import defaultdict


def test_create():
    # dictionaries may contain different types of values
    my_dict = {'one': 1, 'two': 2, 'three': 3.0, 'na': 'not defined'}
    assert len(my_dict) == 4

    # construct a dictionary from two lists
    ids = [78, 79, 98, 102]
    orders = ['TV', 'oven', 'computer', 'bookshelf']
    orders_dict = dict(zip(ids, orders))
    print(orders_dict)

    # construct a dictionary from keys only
    keys = ['x', 'y', 'z']
    point = dict.fromkeys(keys, 1.0)
    assert len(point) == 3
    assert point['y'] == 1.0


def test_empty():
    empty_dict = {}
    if empty_dict:
        print('The dictionary is not empty')
    assert not empty_dict


def test_add_retrieve():
    my_dict = {}
    assert len(my_dict) == 0

    my_dict['four'] = 4   # adding a new value to the dictionary
    assert my_dict['four'] == 4

    my_dict.update(five=5, six=6)  # another way to add values to dictionary
    assert my_dict['five'] == 5
    assert my_dict['six'] == 6

    # insertion order is preserved from Python 3.7+
    for key, value in my_dict.items():  # also:  keys(), values()
        print(key, '->', value)

    # get non-existing element or default
    missing_value = my_dict.get('seven', 'Unknown')  # the default value is None if not specified
    assert missing_value == 'Unknown'


def test_delete():
    my_dict = {'four': 4, 'five': 5}

    del my_dict['four']   # delete the key/value pair
    assert len(my_dict) == 1


def test_dict_to_set():
    my_dict = {'one': 1, 'uno': 1}

    # dict values to set (useful to omit duplicate values)
    my_set = set(my_dict.values())
    assert len(my_set) == 1


def test_defaultdict():
    # a dictionary with default value (requires a producer function with zero args to construct the default value)
    countries = defaultdict(set)          # empty set as the default value
    countries['Japan'] = {'Tokyo', 'Nara'}
    assert countries['Russia'] == set()   # can safely access missing values
