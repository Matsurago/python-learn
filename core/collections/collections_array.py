import array

# array types: i/I - int/unsigned int, b/B - char, h/H - short, l/L - long, f - float, d - double, u - Unicode
a = array.array('I', [1, 2, 3])
print(a)
