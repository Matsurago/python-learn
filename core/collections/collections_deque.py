from collections import deque

q = deque()

q.append(1)
q.append(2)
q.append(3)
assert [1, 2, 3] == list(q)

assert 1 == q.popleft()  # from head
assert 3 == q.pop()      # from tail
