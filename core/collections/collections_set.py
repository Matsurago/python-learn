def test_set_creation():
    # a set contains unique elements
    numbers = {1, 2, 2, 3, 1, 3}
    assert numbers == {1, 2, 3}  # equality compares only elements, not their order, which is undefined

    empty_set = set()
    assert not empty_set

    empty_set.add(5)
    assert empty_set == {5}

    empty_set.remove(5)
    assert not empty_set


def test_operations():
    numbers = {1, 2, 3}

    assert numbers.union({2, 3, 4, 5}) == {1, 2, 3, 4, 5}
    assert numbers | {2, 3, 4, 5} == {1, 2, 3, 4, 5}

    assert numbers.intersection({3, 4}) == {3}
    assert numbers & {3, 4} == {3}

    assert numbers.difference({2, 3}) == {1}
    assert numbers - {2, 3} == {1}

    assert numbers.symmetric_difference({2, 4}) == {1, 3, 4}
    assert numbers ^ {2, 4} == {1, 3, 4}


def test_dictionary_keys():
    # dictionary key view and value view are sets, so set operations work on them
    d1 = {'Minsk': 'Belarus', 'London': 'UK', 'New York': 'USA'}
    d2 = {'London': 'UK', 'Krakow': 'Poland'}
    assert d1.keys() & d2.keys() == {'London'}


def test_frozenset():
    # immutable set (hashable; can be used as a key in dict)
    s2 = frozenset([1, 2, 3, 2, 3, 1])
    assert s2 == {1, 2, 3}
    assert hash(s2)
