from collections import Counter

text = """
The cat is a small carnivorous mammal
It is the only domesticated species in the family Felidae
and often referred to as the domestic cat to distinguish it
from wild members of the family
"""

tokens = text.lower().split()

counter = Counter(tokens)

for word, frequency in counter.items():
    print(f'{word:<18}{frequency}')
