# To implement a custom collection, inherit a base abstract class
# from the 'collections.abc' module. Once the required methods are implemented,
# the rest (such as len and count) are provided for free.
from collections.abc import Sequence


class MyTree(Sequence):
    def __init__(self, value, left, right):
        self.value = value
        self.left = left
        self.right = right

    def __len__(self) -> int:
        result = 0
        for _ in self.traverse():
            result += 1
        return result

    def __getitem__(self, index: int):
        for i, node in enumerate(self.traverse()):
            if i == index:
                return node.value
        raise IndexError(f'Index {index} is out of range')

    def traverse(self):
        yield self
        if self.left:
            yield from self.left.traverse()
        if self.right:
            yield from self.right.traverse()


tree = MyTree(3, left=MyTree(2, None, None), right=MyTree(7, None, MyTree(9, None, None)))

print(tree[0])
print(tree[1])
print(tree[2])

print(list(tree))
print(f'Number of nodes = {len(tree)}')
