import itertools
import random


# iterator slice
it = range(10)
print('initial list:', list(it))

it_slice = itertools.islice(it, 2, 9, 2)  # every 2nd element from 2 to 8
print('slice:', list(it_slice))

# infinite cycle
it = itertools.cycle([1, 2, 3])
print('cycle:', list(itertools.islice(it, 7)))

# infinite repeat
it = itertools.repeat(5)
print('repeat', list(itertools.islice(it, 7)))

# combine multiple iterators
it = itertools.chain([1, 2, 3], [4, 5], range(6, 8))
print('chain:', list(itertools.islice(it, 7)))

# process the same data via multiple iterators
it1, it2, it3 = itertools.tee([1, 2], 3)
print('tee:', list(it1), list(it2), list(it3))

# zip using the longest sequence (default zip uses the shortest one)
it = itertools.zip_longest([1, 2], ['a', 'b', 'c', 'd'], fillvalue=0)
print('zip:', list(it))  # [(1, 'a'), (2, 'b'), (0, 'c'), (0, 'd')]

# take and drop while
it = itertools.takewhile(lambda x: x < 7, range(10))
print('takewhile:', list(it))
it = itertools.dropwhile(lambda x: x < 7, range(10))
print('dropwhile:', list(it))

# filter on negated condition
it = itertools.filterfalse(lambda x: x % 3 == 0, range(10))
print('filterfalse:', list(it))  # all not divisible by 3

# fold left saving all intermediate results
it = itertools.accumulate(range(1, 10), lambda x, y: x * y)
print('accumulate:', list(it))

# random walk with accumulate
steps = random.choices([-1, 1], k=20)
random_walk = [0] + list(itertools.accumulate(steps))
print('random walk:', random_walk)

# cartesian product
it = itertools.product([1, 2], ['a', 'b'])
print('cartesian product:', list(it))  # [(1, 'a'), (1, 'b'), (2, 'a'), (2, 'b')]
it = itertools.product([1, 2], repeat=2)  # with self
print('cartesian product (with self):', list(it))  # [(1, 1), (1, 2), (2, 1), (2, 2)]

# all permutations of length N
it = itertools.permutations([1, 2, 3, 4], 3)
print('permutations:', list(it))

# all combinations of elements of length N, elements are not repeated
it = itertools.combinations([1, 2, 3, 4], 3)
print('combinations:', list(it))
it = itertools.combinations_with_replacement([1, 2, 3, 4], 3)  # with repetitions
print('combinations with replacement:', list(it))
