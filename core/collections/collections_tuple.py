# a tuple is an immutable list
from collections import namedtuple

tup = (16, 25, 36, '49', [64, 81])  # can contain elements of different types

# tuples can be iterated the same way as lists
for value in tup:
    print(value, end=' ')

# tuple elements can be accessed like in a list
print('\nSecond element:', tup[1])

# named tuple is a lightweight immutable class
Person = namedtuple('Person', ('name', 'age'))
person = Person('Vasya', 18)
print(f"My name is {person.name} and I'm {person.age} years old.")
