import locale
import unicodedata

# know your default encoding, it may be not Unicode
print('default encoding:', locale.getpreferredencoding())

# input unicode characters:
# \uPPii  PP - plane, ii - index;  00 plane is ASCII
print('Euro sign:', '\u20ac')
# \Uxxxxyyyy  for characters in higher planes
print('Clover:', '\U0001f340')
# \N{name}  by name
print('Coffee:', '\N{Hot Beverage}')
# by index
print('Greek letter:', chr(0x1fc6))

for i in range(0x1f00, 0x1fff):
    c = chr(i)  # convert a number to unicode character
    try:
        name = unicodedata.name(c)  # get unicode character name (use lookup() to get char by its name)
        category = unicodedata.category(c)  # get unicode character category (e.g. Lu = latin upper case)
        print(f'{i} {c} {name} {category}')
    except ValueError:
        print(f'{i} no character at this index!')


# convert to ASCII
s = 'Tokyo 東京'
print(f'Unicode: {s}, ASCII: {ascii(s)}')

# normalize same characters with different representations
spanish_n1 = '\xf1'
spanish_n2 = 'n\u0303'  # same but as a composition of symbols 'n' and '~'
assert spanish_n1 != spanish_n2
assert unicodedata.normalize('NFC', spanish_n1) == unicodedata.normalize('NFC', spanish_n2)
