import logging

# add filename='my.log' parameter to log to a file
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.DEBUG)

logging.debug('a simple entry')
logging.debug('value = %s' % 7)
