import base64

# bytes: binary literal (immutable)
binary_data = b'\x61\x62\x63'
print(binary_data)

# bytes: from a collection of bytes (immutable)
data2 = bytes([0x64, 0x65, 0x66])
print(data2)

# mutable byte array
data3 = bytearray()
data3.extend([0x67, 0x68, 0x69])
data3.append(0x6a)
print(data3)

# use memoryview to get a slice of bytes without copying them (good for huge byte arrays, as in a video)
data = b'a very large binary array'
view = memoryview(data)
chunk = view[7:12]
print('chunk size:', chunk.nbytes)
print('chunk data:', chunk.tobytes())
print('whole data:', chunk.obj)

# bytes to string
text = binary_data.decode('utf-8')
print(text)

# string to bytes
result = 'some text'.encode('utf-8')
print(result)

# bytes to HEX string (use binascii.b2a_hex() to convert to HEX bytes)
result = b'hello'.hex()
print('bytes to hex:', result)

# HEX string to bytes
result = bytes.fromhex('68656c6c6f')
print('hex to bytes:', result)

# Base64 encoding
result = base64.b64encode(b'hello')
print('base64 encode:', result)

# Base64 decoding
result = base64.b64decode('aGVsbG8=')
print('base64 decode:', result)

# encode/decode error modes:
# - strict:  raises exception on incorrect data (default)
# - ignore:  ignores incorrect chars
# - replace: replaces incorrect chars with \U+FFFD or ?
# - surrogateescape: replaces any incorrect char \xhh with U+DChh on decode, and v.v. on encode
# - backslashreplace: <encode only> replaces incorrect chars with their code (i.e. \u1234)
# - xmlcharrefreplace: <encode only> replaces incorrect chars with their XML code (i.e. &#4660;)

kanji = 'city: 東京'.encode('utf-8')
ascii_result = kanji.decode('ascii', errors='replace')
print(ascii_result)

# write to a file
# can set the buffer size with 'buffering' kwarg (set to 0 to disable buffering)
with open('binary.data', 'wb') as file:  # must use 'b' mode
    file.write(b'\x61\x62\x63\x64\x65')

# binary representation of a number
print(bin(31))
