# use Decimal class for predictable floating point values
from decimal import Decimal, ROUND_UP

x = Decimal('0.1')
y = Decimal('0.2')
print(x + y)

# rounding
z = Decimal('0.00413')
rounded = z.quantize(Decimal('0.01'), rounding=ROUND_UP)
print(z)

# compound interest example
principal = Decimal('1000')  # initial amount
rate = Decimal('0.05')

for year in range(1, 11):
    amount = principal * (1 + rate) ** year
    print(f'{year:>2}{amount:>10.2f}')
