from dataclasses import dataclass
from typing import ClassVar


@dataclass(order=True)
class Point:
    # class attributes, special data type is required
    origin: ClassVar[float] = 0

    # data attributes are also defined outside init, data types are required
    x: float
    y: float


# inheritance
@dataclass
class Point3D(Point):
    z: float


if __name__ == '__main__':
    # __init__, __repr__, __eq__ are always auto-generated
    # comparison operators like __gt__ are generated when order=True
    p = Point(3, 7)
    print(p)  # __repr__

    print(p == Point(3, 7))  # __eq__
    print(p < Point(4, 6))   # __lt__

    # inheritance
    v = Point3D(1.0, 2.0, 3.0)
    print(v)
