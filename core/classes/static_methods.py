# static methods are useful for polymorphism to avoid creating object instances
class DoublePolicy:
    @staticmethod
    def advance(x):
        return x * 2


class PowerPolicy:
    @staticmethod
    def advance(x):
        return x ** 2


class Amount:
    def __init__(self, initial_value=1, policy=DoublePolicy):
        self.value = initial_value
        self.policy = policy

    def step(self):
        self.value = self.policy.advance(self.value)


def test_method_polymorphism():
    amount = Amount()
    amount.step()
    amount.step()
    assert amount.value == 4

    amount.policy = PowerPolicy
    amount.step()
    amount.step()
    assert amount.value == 256
