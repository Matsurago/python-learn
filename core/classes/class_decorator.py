import inspect


# example of a class decorator
def my_decorator(cls):
    for key in dir(cls):
        value = getattr(cls, key)

        # can modify methods and class attributes here
        print(f'{key} -> {value}')

        # modify class attributes
        if isinstance(value, int):
            value *= 100
            setattr(cls, key, value)

    # modify behavior of existing function
    orig_foo = cls.foo

    def updated_foo(self):
        return orig_foo(self).upper()

    cls.foo = updated_foo

    # can generate new code (i.e. __repr__)
    attrs = inspect.signature(cls).parameters
    attrs_str = ', '.join('{self.%s!r}' % attr for attr in attrs)
    code = f'def __repr__(self):\n  return f"{cls.__name__}({attrs_str})"\n'
    local_vars = {}
    exec(code, local_vars)  # warning: low performance
    cls.__repr__ = local_vars['__repr__']

    return cls  # must return the class


@my_decorator
class MyClass:
    VAL = 7

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def foo(self):
        return 'test'


if __name__ == '__main__':
    obj = MyClass(2, 3)
    print(obj.VAL)
    print(obj.foo())
    print(obj)
