class Employee:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    @property
    def age(self):
        return self._age

    # this method is also invoked during construction of the object
    @age.setter
    def age(self, age):
        if age < 0:
            raise ValueError(f"Expected age > 0, got {age}")
        self._age = age

    # computed property (read-only)
    @property
    def salary(self):
        return 1000 * self.age


if __name__ == '__main__':
    employee = Employee("George", 30)
    print(employee.salary)

    try:
        employee.age = -1  # triggers exception
    except ValueError as e:
        print(e)

    try:
        employee = Employee("George", -10)  # triggers exception
    except ValueError as e:
        print(e)
