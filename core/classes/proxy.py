class Service:
    def greet(self):
        print('hello')


class LoggingProxy:
    def __init__(self, base):
        self._base = base

    def __getattr__(self, item):
        print(f'[INFO] Accessing "{item}"')
        return getattr(self._base, item)  # propagate call to the wrapped class (doesn't work for magic methods!)


if __name__ == '__main__':
    service = LoggingProxy(Service())
    service.greet()
