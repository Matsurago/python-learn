# metaclasses let you intercept Python’s class statement
# and provide special behavior each time a class is defined (sic!)
import inspect


# must inherit from 'type'
class MyMeta(type):

    # creates the class namespace
    @classmethod
    def __prepare__(mcs, cls_name, bases):
        print()
        print(f'Preparing {cls_name} class namespace')
        return super().__prepare__(cls_name, bases)

    # creates the class
    @staticmethod
    def __new__(mcs, cls_name, bases, cls_dict):
        print(f'Running {mcs}.__new__ for {cls_name}')
        print(f'Base classes: {bases}')
        print(f'Class dictionary: {cls_dict}')

        # can add validation for class attributes (not instance attributes as they're per instance)
        if cls_dict.get('amount'):
            raise ValueError('Forbidden class attribute name: amount.')

        # can modify the class (example: create __slots__ based on __init__):
        if '__init__' in cls_dict:
            signature = inspect.signature(cls_dict['__init__'])
            __slots__ = tuple(signature.parameters)[1:]
        else:
            __slots__ = ()
        cls_dict['__slots__'] = __slots__

        return super().__new__(mcs, cls_name, bases, cls_dict)

    # initializes the class
    def __init__(cls, cls_name, bases, namespace):
        print('Initializing:', cls_name, bases, namespace)
        super().__init__(cls_name, bases, namespace)

    # creates instances of the class
    def __call__(cls, *args, **kwargs):
        print('Creating instance:', args, kwargs)
        return super().__call__(*args, **kwargs)


class MyClass(metaclass=MyMeta):
    x = 7

    def __init__(self, y):
        self.y = y

    def foo(self):
        pass


# all subclasses will have the same metaclass as the base
class MySubclass(MyClass):
    y = 3

    def bar(self):
        pass


if __name__ == '__main__':
    print('in main')

    c = MyClass(5)
    print(dir(c))  # no __dict__ since we created __slots__
