class Field:
    def __init__(self):
        # set automatically by __set_name__
        self.name = None
        self.internal_name = None

    def __get__(self, instance, owner):
        if instance is None:
            return self
        return getattr(instance, self.internal_name)

    def __set__(self, instance, value):
        setattr(instance, self.internal_name, value)

    # called when a descriptor is defined for a class attribute
    def __set_name__(self, owner, name):
        self.name = name
        self.internal_name = '_' + name


class Customer:
    first_name = Field()
    last_name = Field()
    address = Field()


if __name__ == '__main__':
    alice = Customer()
    alice.first_name = 'Alice'
    print(alice.__dict__)

    bob = Customer()
    bob.first_name = 'Bob'
    bob.last_name = 'Wilson'
    print(bob.__dict__)

    print(alice.__dict__)
