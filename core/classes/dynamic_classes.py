import types


def __init__(self, name):
    self.name = name


def greet(self):
    print(f'hello {self.name}')


if __name__ == '__main__':
    methods = {
        '__init__': __init__,
        'greet': greet
    }

    # create a class
    # second arg is base classes
    Greeter = types.new_class('Greeter', (), exec_body=lambda ns: ns.update(methods))

    greeter = Greeter('Vasya')
    greeter.greet()
