from dataclasses import dataclass
import json


class JsonSerializable:
    # use class methods for object factories
    @classmethod
    def from_json(cls, data):
        obj_dict = json.loads(data)
        # noinspection PyArgumentList
        return cls(**obj_dict)

    def to_json(self):
        return json.dumps(self.__dict__)  # wont work for nested objects


@dataclass
class Cat(JsonSerializable):
    name: str
    age: int
    broken_vases: int


if __name__ == '__main__':
    cat_json = """
    {
        "name": "Tama",
        "age": 3,
        "broken_vases": 10
    }
    """
    cat = Cat.from_json(cat_json)
    print(cat)
    print(cat.to_json())
