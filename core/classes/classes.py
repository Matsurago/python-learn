"""A class module should have a top-level doc comment"""


class Cat:
    """A simple cat class."""

    def __init__(self, name, age):
        self.name = name         # fields are called "attributes" in Python
        self.age = age
        self.broken_vases = 0    # default attribute

    def greet(self):
        print(self.name, 'greets you with meowing!')

    def dance(self):
        # this method should be implemented in a subclass
        raise NotImplementedError

    # use class methods for object factories
    @classmethod
    def generic_cat(cls):
        return cls('White', 3)

    # same as .toString() in Java
    def __str__(self):
        return f'{self.name}, {self.age} y.o.'

    # repr should return a valid constructor invocation, which can be passed to eval()
    def __repr__(self):
        # __qualname__ gives the simple name of the class
        return f'{self.__class__.__qualname__}(name={self.name}, age={self.age})'


class ScottishFold(Cat):
    """A subclass of Cat"""

    def __init__(self, name, age):
        super().__init__(name, age)
        self.fold_ratio = 0.5

    def dance(self):
        print('A scottishfold kitten beautifully dances.')


if __name__ == '__main__':
    cat = ScottishFold('White', 7)
    cat.greet()
    cat.dance()

    another_cat = ScottishFold.generic_cat()
    print(another_cat)
