from abc import ABC, abstractmethod


# abstract classes are useful to implement interfaces
class Greeter(ABC):
    @abstractmethod
    def greet(self):
        pass


class FancyGreeter(Greeter):
    def greet(self):
        print('hi there!')


if __name__ == '__main__':
    # can't instantiate an abstract class
    # greeter = Greeter()

    fancy_greeter = FancyGreeter()
    fancy_greeter.greet()
