# to create an enum, subclass the enum.Enum class
import enum


class Color(enum.Enum):
    cyan = 1
    magenta = 2
    yellow = 3


if __name__ == '__main__':
    print(isinstance(Color.magenta, Color))
