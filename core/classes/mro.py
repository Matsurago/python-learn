class Base:
    pass


class A(Base):
    pass


class B(Base):
    pass


class Diamond(A, B):
    pass


# handler pattern based on method resolution order (MRO)
class Dispatcher:
    def handle(self, obj):
        for cls in type(obj).__mro__:
            handler = getattr(self, f'handle_{cls.__name__}', None)
            if handler:
                return handler(obj)
        raise ValueError(f'No handler for {type(obj)}.')

    def handle_A(self, obj):
        print('handler for class A')

    def handle_B(self, obj):
        print('handler for class B')

    def handle_Diamond(self, obj):
        print('handler for class Diamond')


if __name__ == '__main__':
    diamond = Diamond()
    print(Diamond.__mro__)  # order: A, B, Base, object

    dispatcher = Dispatcher()
    dispatcher.handle(Diamond())
    dispatcher.handle(B())
    dispatcher.handle(A())
