class Decoder:
    mimetype = None
    registry = {}

    def __init_subclass__(cls):
        if not cls.mimetype:
            raise ValueError(f'Decoder subclass {cls.__qualname__} must define attribute "mimetype".')

        Decoder.registry[cls.mimetype] = cls

        super().__init_subclass__()  # don't forget to propagate


class ImageDecoder(Decoder):
    mimetype = 'image/jpeg'


if __name__ == '__main__':
    print(Decoder.registry)
