from datetime import datetime


# Object attributes can be generated on demand (useful for ORM, parsing, etc.)
class DynamicAttrDemo:
    # called only when attribute doesn't exist
    def __getattr__(self, name):
        print(f'> setting value for attribute {name}')
        value = f'default value for {name}'
        setattr(self, name, value)
        return value

    # called each time attribute is accessed
    def __getattribute__(self, name):
        print(f'> accessing attribute {name}')
        return super().__getattribute__(name)

    # called each time attribute is assigned
    def __setattr__(self, key, value):
        print(f'> setting {key} = {value!r}')
        super().__setattr__(key, value)


if __name__ == '__main__':
    x = DynamicAttrDemo()
    print(x.__dict__)  # empty

    print(x.foo)  # would have thrown AttributeError without __getattr__
    print(x.__dict__)

    print(hasattr(x, 'bar'))  # also triggers __getattr__
    print(x.__dict__)

    # another option is to get a default value if an attribute doesn't exist:
    y = datetime.now()
    print(getattr(y, 'year'))  # existing attribute
    print(getattr(y, 'century', 'n/a'))  # non-existing attribute, with default doesn't raise exception
