from weakref import WeakKeyDictionary


class Grade:
    """Descriptors are useful for validation"""

    def __init__(self):
        self._values = WeakKeyDictionary()

    # must be defined to satisfy descriptor protocol
    def __get__(self, instance, owner):
        if instance is None:
            return self
        return self._values.get(instance, 0)

    # must be defined to satisfy descriptor protocol
    def __set__(self, instance, value):
        if not isinstance(value, (int, float)):
            raise TypeError(f'Expected {value!r} to be an int or a float')

        if value < 0 or value > 100:
            raise ValueError(f'Expected 0 <= grade <= 100, got {value!r}')

        self._values[instance] = value


class Exam:
    # must be class attributes (not object attributes)
    math_grade = Grade()
    chemistry_grade = Grade()


if __name__ == '__main__':
    exam = Exam()
    # we access through object attribute
    # __getattr__ for object fails => __getattribute__ for class
    exam.math_grade = 78
    print(exam.math_grade)

    try:
        exam.chemistry_grade = 102  # triggers exception
    except ValueError as e:
        print(e)

    print(exam.chemistry_grade)

    # with proper implementation, different objects will have different values
    exam2 = Exam()
    exam2.math_grade = 89
    assert exam2.math_grade != exam.math_grade
