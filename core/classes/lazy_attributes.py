# lazy attributes can be implemented with descriptors
class Lazy:
    def __init__(self, f):
        self.f = f

    def __set_name__(self, cls, name):
        self.name = name

    # descriptor is invoked only if there is no instance attribute (=> invoked once)
    def __get__(self, instance, owner):
        print(f'lazy access for {self.name}')

        if instance is None:
            return self

        value = self.f(instance)
        instance.__dict__[self.name] = value
        return value


class Rectangle:
    def __init__(self, width, height):
        self.width = width
        self.height = height

    area = Lazy(lambda self: self.width * self.height)
    perimeter = Lazy(lambda self: 2 * (self.width + self.height))


if __name__ == '__main__':
    rect = Rectangle(3, 4)
    print(rect.__dict__)

    print(rect.area)
    print(rect.perimeter)
    print(rect.__dict__)

    print(rect.area)  # this time attribute is used
