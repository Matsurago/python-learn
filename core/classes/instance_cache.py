"""
Cache instances to avoid creating same objects.
"""
import weakref


class Date:
    _cache = {}

    @staticmethod
    def __new__(cls, year, month, day):
        self_ref = Date._cache.get((year, month, day))
        if self_ref:
            self = self_ref()  # call a weak reference to get the object
        else:
            self = super().__new__(cls)
            self.year = year
            self.month = month
            self.day = day
            Date._cache[year, month, day] = weakref.ref(self)
        return self

    def __init__(self, year, month, day):
        pass

    # invoked when reference count for an object reaches zero
    def __del__(self):
        del Date._cache[self.year, self.month, self.day]


if __name__ == '__main__':
    print(Date._cache)

    x = Date(2017, 1, 1)
    print(Date._cache)

    y = Date(2017, 1, 1)
    assert x is y

    del x
    print(Date._cache)

    del y
    print(Date._cache)
