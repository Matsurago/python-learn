import locale

# get all supported locales
keys = locale.locale_alias.keys()
locales = [key for key in keys if len(key) == 5 and key[2] == '_']

print(locales)
