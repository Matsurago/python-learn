import warnings


# a new parameter to established API can be added as a keyword parameter with default value = None
def foo(x, y, new_param=None):
    if new_param is None:
        # warn if a caller uses the old function signature
        warnings.warn('new_param is required', DeprecationWarning)
        new_param = 1
    return new_param * (x + y)


print(foo(3, 4))  # issues deprecation warning
