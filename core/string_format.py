# float formatting :column_width.num_decimal_digits
print(f'{1.2934:10.2f}|')
print(f'{1.2934:10.2e}|')  # scientific notation
# left alignment
print(f'{1.2934:<10.2f}|')
# center alignment
print(f'{1.2934:^10.2f}|')
# format expressions themselves can be parameterized
decimal_places = 1
print(f'{1.2934:^10.{decimal_places}f}|')

# force + sign and 3 leading zeroes
print(f'{123:+07d}')
# force space instead of + sign
print(f'{12: d}\n{-12: d}')

# format with thousands separator
print(f'{1024768.99:,.2f}')

# format a number in binary or hex
a = 255
print(f'dec = {a}, hex = {a:x}, bin = {a:b}, oct = {a:o}')
