# binary
x = 0b1010
assert x == 10

# hexadecimal
y = 0xff
assert y == 255

# convert a number to its binary representation
assert bin(10) == '0b1010'

# convert a number to its hexadecimal representation
assert hex(127) == '0x7f'

# float to integer
assert 3 == int(3.5)
assert 4 == round(3.5)
