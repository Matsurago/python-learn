import math

print(3 * 10**6)  # pow
print(.2 + .1)    # can omit 0
print(1 / 2)      # division yields floats by default

d, rem = divmod(71, 11)  # get int division result and remainder in one op
print(f'{d} * 11 + {rem} = 71')

print(round(7.28372, 3))  # round to n digits

# degrees to radians and v.v.
print('180 grad. =', math.radians(180))
print('pi rad. =', math.degrees(math.pi))

# a complex number
z = complex(3, 4)
print(f'z = {z}, conjugate of z = {z.conjugate()}')
