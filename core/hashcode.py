
s1 = [1, 2, 3]
print(id(s1))

s2 = s1      # reference to the same object
print(id(s2))

s3 = s1[:]   # a copy
print(id(s3))
