import sqlite3
from dataclasses import dataclass


@dataclass
class Employee:
    id: int
    name: str
    salary: int


if __name__ == '__main__':
    employees = [
        Employee(1, 'John', 3000),
        Employee(2, 'Ann', 4000),
        Employee(3, 'Stephanie', 5000),
    ]

    with sqlite3.connect('my_database.db') as connection, connection.cursor() as cursor:
        # noinspection SqlNoDataSourceInspection
        cursor.execute('DROP TABLE IF EXISTS employee')

        # noinspection SqlNoDataSourceInspection
        cursor.execute('''
        CREATE TABLE employee (
            id SERIAL PRIMARY KEY,
            name VARCHAR(32) NOT NULL,
            salary INT NOT NULL
        )
        ''')

        for employee in employees:
            # noinspection SqlNoDataSourceInspection,SqlResolve
            cursor.execute('INSERT INTO employee (id, name, salary) VALUES (?, ?, ?)',
                           (employee.id, employee.name, employee.salary))

        # noinspection SqlResolve
        cursor.execute('SELECT * FROM employee')
        rows = cursor.fetchall()
        print(rows)
