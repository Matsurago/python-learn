import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

connection = sa.create_engine('postgresql://alex:123@localhost/main')

# Low-level access
rows = connection.execute('SELECT * FROM employees')
for row in rows:
    print(row)

# Intermediate-level access
metadata = sa.MetaData(bind=connection)
metadata.reflect()
employees_table = metadata.tables['employees']

result = connection.execute(employees_table.select())
rows = result.fetchall()
print(rows)


# High-level access (ORM)
Base = declarative_base()


class Song(Base):
    __tablename__ = 'songs'

    id = sa.Column('id', sa.Integer, primary_key=True)
    name = sa.Column('name', sa.String)

    def __init__(self, name):
        super().__init__()
        self.name = name

    def __repr__(self):
        return self.name


# create songs table
Base.metadata.create_all(connection)

# insert rows
Session = sessionmaker(bind=connection)
session = Session()

session.add(Song(name='To be human'))
session.add(Song(name='Savages'))

session.commit()
