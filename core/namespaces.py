from types import SimpleNamespace


# global variables live in global() namespace
x = 100
print(globals())
print(globals()['x'])

# changing the value in the namespace changes the variable
globals()['x'] = 101
print(x)

# a custom namespace
ns = SimpleNamespace(x=99, y=100, z=200)
# now can easily access variables from the namespace
ns.y = 1000
print(ns.x, ns.y)
