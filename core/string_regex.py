import itertools
import re


def test_split():
    assert re.split(r',\s*', '1,2, 3,  4') == ['1', '2', '3', '4']


def test_search_precompile():
    s = 'Register email: test@test.com.'
    email_regex = re.compile(r'\w+@\w+.\w+')

    # search finds only first occurrence
    result = email_regex.search(s)
    assert result.group() == 'test@test.com'


def test_search_non_greedy():
    greedy_result = re.search(r'.*\.py', 'file.py.py')
    assert greedy_result.group() == 'file.py.py'

    greedy_result = re.search(r'.*?\.py', 'file.py.py')
    assert greedy_result.group() == 'file.py'


def test_full_match():
    # digit
    assert re.fullmatch(r'\d{4}', '0123')
    assert not re.fullmatch(r'\d{5}', '0123')

    # not a digit
    assert re.fullmatch(r'\D{4}', 'abcd')
    assert not re.fullmatch(r'\D{4}', 'ab3d')
    assert not re.fullmatch(r'\D{4}', 'abcde')

    # any whitespace
    assert re.fullmatch(r'\s*', '   \t')
    assert re.fullmatch(r'\s*', '')
    assert not re.fullmatch(r'\s*', 'a')

    # not a whitespace
    assert re.fullmatch(r'\S+', 'a')

    # any word (letter, digit, underscore)
    # use \W for not a word
    assert re.fullmatch(r'\w+', 'Welt_935')
    assert not re.fullmatch(r'\w+', 'Welt 935')

    # match at least n digits
    assert re.fullmatch(r'-?\d{3,}', '-1024')
    assert not re.fullmatch(r'-?\d{3,}', '-10')

    # match between n and m digits
    assert re.fullmatch(r'-?\d{3,5}', '-1024')
    assert not re.fullmatch(r'-?\d{3,5}', '-102355')


def test_classes():
    # note: * and + are greedy (match as many characters as possible)
    assert re.fullmatch(r'[A-Z][a-z]*', 'Ivan')
    assert not re.fullmatch(r'[A-Z][a-z]*', 'anna')

    # ^ = not
    assert re.fullmatch(r'[^a-z]*', 'EVA')
    assert not re.fullmatch(r'[^a-z]*', 'Henry')

    # special characters are treated as escaped within brackets
    assert re.fullmatch(r'[+$]+\d+', '+980')
    assert re.fullmatch(r'[+$]+\d+', '$980')


def test_flags():
    assert not re.fullmatch(r'[a-z]*', 'heLGa')
    assert re.fullmatch(r'[a-z]*', 'heLGa', flags=re.IGNORECASE)


def test_replace():
    # replace all with re.sub(pattern, replacement, orig_string)
    result = re.sub(r'\d+', r'', '1234 test 12 x 1 2')
    assert result == ' test  x  '

    # can specify count parameter to limit the number of replacements
    result = re.sub(r'\d+', r'', '1234 test 12 x 1 2', count=1)
    assert result == ' test 12 x 1 2'

    # can use the matched string in the replacement
    result = re.sub(r'ID:(\d+)', r'\1', 'tableware ID:123, shipment ID:9')
    assert result == 'tableware 123, shipment 9'

    # can replace each time with a different string
    i = itertools.count(1)
    result = re.sub(r'xx', lambda exp: f'{exp.group()}-{next(i)}', 'first: xx, second: xx, third: xx')
    assert result == 'first: xx-1, second: xx-2, third: xx-3'


def test_search():
    assert re.search(r'\$\d+', 'Total is $190.')
    assert not re.search(r'\$\d+', 'Total is 190.')

    # ^: match only at the beginning of the string
    assert re.search(r'^cat', 'cat is cute')
    assert not re.search(r'^cat', 'That cat is cute')

    # $: match only at the end of the string
    assert re.search(r'cat$', 'This is my cat')
    assert not re.search(r'cat$', 'These are my cat and dog')

    # match only if followed by a particular string
    assert re.search(r'cat (?=White)', 'This is my cat White.')
    assert not re.search(r'cat (?=White)', 'This is my cat Tama.')

    # match only if preceded by a particular string
    assert re.search(r'(?<=my) cat', 'This is my cat White.')
    assert not re.search(r'(?<=my) cat', 'This is your cat White.')

    # match words that begin with a prefix (\b for word boundary)
    assert re.search(r'\bcomp', 'a new computer')
    assert not re.search(r'\bcomp', 'a new accomplishment')


def test_search_newline():
    # by default, .* matches everything but a newline
    result = re.search(r'.*', 'line 1\nline 2')
    assert result.group() == 'line 1'

    # to match a newline, pass the re.DOTALL parameter
    result = re.search(r'.*', 'line 1\nline 2', re.DOTALL)
    assert result.group() == 'line 1\nline 2'


def test_findall():
    # without capture groups: returns a list of all matches
    result = re.findall(r'\d+', 'The error codes are 1024, 713, and 19.')
    assert result == ['1024', '713', '19']

    # with capture groups: returns a list of tuples
    result = re.findall(r'(\d{3})-(\d{4})', 'call 123-4567 or 899-1122')
    assert result == [('123', '4567'), ('899', '1122')]


def test_capture_group():
    result = re.search(r'(\d{3})-(\d{4}-\d{4})', 'Tel: 777-8888-9999')
    assert result is not None

    assert result.group(0) == '777-8888-9999'  # entire match
    assert result.group(1) == '777'
    assert result.group(2) == '8888-9999'
    assert result.groups() == ('777', '8888-9999')  # all groups


def test_named_capture_group():
    result = re.search(r'(?P<AREA>\d{3})-(?P<NUMBER>\d{4}-\d{4})', 'Tel: 777-8888-9999')

    assert result.group('AREA') == '777'
    assert result.group('NUMBER') == '8888-9999'


def test_regex_with_comments():
    # pass the re.VERBOSE flag
    tel_regex = re.compile(r'''
        \d{2,3}   # mobile operator code or landline
        -
        \d{4}     # number, part 1
        -
        \d{4}     # number, part 2
    ''', re.VERBOSE)

    assert tel_regex.fullmatch('03-1111-2222')
