import html
from html.entities import html5


def test_html_unescape():
    # get symbol by its HTML name
    assert html.unescape('&amp;') == '&'

    # get symbol by its number
    assert html.unescape('&#233;') == 'é'

    # get symbol by its hex number
    assert html.unescape('&#xe9;') == 'é'

    # another way to lookup by name
    assert html5['amp'] == '&'


def test_html_escape():
    s = 'café'
    s_escaped_bytes = s.encode('ascii', errors='xmlcharrefreplace')
    result = s_escaped_bytes.decode()

    assert result == 'caf&#233;'
