import sys

print('Args count:', len(sys.argv))

for i, arg in enumerate(sys.argv, 1):
    print(f'{i}: {arg}')  # as usual, the first arg is the script filename (absolute path)
