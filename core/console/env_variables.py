import os

if __name__ == '__main__':
    path = os.environ.get('PATH')
    pwd = os.environ.get('PWD')

    print('PATH =', path)
    print('PWD =', pwd)

    # can also set an env variable
    # this affects all apps launched with subprocess.run(..)
    os.environ['PWD'] = '/home/user'
