import argparse
import sys

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='A sample app')
    parser.add_argument('path')  # required argument
    parser.add_argument('-o', '--output', action='store')  # named arg
    parser.add_argument('-v', '--verbose', action='store_true', default=False)

    args = parser.parse_args(sys.argv[1:])

    print('output:', args.output)
    print('verbose:', args.verbose)
    print('path:', args.path)
