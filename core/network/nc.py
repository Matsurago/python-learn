import socket
import sys

BUF_SIZE = 4096


def main(address, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((address, port))
        while True:
            data = input()
            s.send(data.encode())

            response = s.recv(BUF_SIZE)
            print(response.decode())


def parse_port(port):
    try:
        port = int(port)
    except:
        bad_port = True
    else:
        bad_port = (port < 0) or (port > 65535)

    if bad_port:
        print(f"Incorrect port number: '{port}'")
        sys.exit(1)

    return port


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: nc [address] [port]')
        sys.exit(0)

    address_ = sys.argv[1]
    port_ = parse_port(sys.argv[2])

    main(address_, port_)
