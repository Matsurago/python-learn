import socket
import sys
import threading

SERVER_IP = '0.0.0.0'
DEFAULT_PORT = 7777
BUF_SIZE = 4096
MAX_BACKLOG_CONNECTIONS = 5


def start_server(port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((SERVER_IP, port))
        s.listen(MAX_BACKLOG_CONNECTIONS)
        print(f'Listening on {SERVER_IP}:{port}')

        while True:
            client_socket, client_addr = s.accept()
            print(f'Accepted connection from {client_addr}')

            client_thread = threading.Thread(target=handle_client, args=(client_socket, client_addr))
            client_thread.start()


def handle_client(client_socket, client_addr):
    with client_socket:
        client_socket.send(f'hello {client_addr}\n'.encode('utf-8'))
        response = client_socket.recv(BUF_SIZE)
        print(f'Response from {client_addr}:', response.decode('utf-8'))


if __name__ == '__main__':
    port_ = DEFAULT_PORT if len(sys.argv) == 1 else int(sys.argv[1])
    start_server(port_)
