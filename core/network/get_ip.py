import socket


hostname = socket.gethostname()
print('Hostname:', hostname)

for address in socket.getaddrinfo(hostname, None):
    family, s_type, proto, canon_name, sock_address = address
    ip_address, *rest = sock_address

    print(f'{family.name:10} {ip_address}')
