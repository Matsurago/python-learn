import socket

BUF_SIZE = 4096


def connect_and_send(addr: str, port: int, data: str):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((addr, port))

        s.send(data.encode())
        response = s.recv(BUF_SIZE)

        return response.decode()


if __name__ == '__main__':
    request_ = 'GET / HTTP/1.1\r\nHost: www.google.com\r\n\r\n'
    response_ = connect_and_send('www.google.com', 80, request_)
    print(response_)
