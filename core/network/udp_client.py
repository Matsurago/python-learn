import socket

BUF_SIZE = 4096


def connect_and_send(addr: str, port: int, data: str):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.sendto(data.encode(), (addr, port))
        response, c_addr = s.recvfrom(BUF_SIZE)
        return response.decode()


if __name__ == '__main__':
    response_ = connect_and_send('127.0.0.1', 7777, 'hello\n')
    print(response_)
