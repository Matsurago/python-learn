import smtplib


server = smtplib.SMTP('smtp.test.com', 587)  # use .SMTP_SSL() for port 465
server.ehlo()  # establish connection, returns 250 for Success
server.starttls()  # no need for SSL/465
server.login('user@test.com', 'password')
# message must have the format "Subject: XX\nMessage Text"
server.sendmail('from@example.com', 'to@example.com', 'Subject: test\nTest email.')
server.quit()
