import asyncio
import socket

HOST = '0.0.0.0'
PORT = 7777
BUF_SIZE = 4096
MAX_CONN = 5


async def server():
    loop = asyncio.get_running_loop()

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((HOST, PORT))
        s.listen(MAX_CONN)
        s.setblocking(False)  # non-blocking socket
        print(f'[*] Server started on {HOST}:{PORT}')

        while True:
            client, addr = await loop.sock_accept(s)
            print('[*] Accepted connection from %s:%d' % addr)
            loop.create_task(handle_client(client))


async def handle_client(client):
    loop = asyncio.get_running_loop()
    with client:
        while True:
            request = await loop.sock_recv(client, BUF_SIZE)
            if not request:
                break

            response = f'You sent me: {request.decode()}'
            await loop.sock_sendall(client, response.encode())


if __name__ == '__main__':
    asyncio.run(server())
