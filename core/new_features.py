from math import cos, radians

seeds = {
    'sesame': 1,
    'melon': 10,
}

# [Python 3.8+] walrus operator, assignment within statements
if melon_seeds := seeds.get('melon', 0):
    print(f"There are {melon_seeds} melon seeds.")
else:
    print("There are none.")

# [Python 3.8+] output key=value with = parameter for f-strings
theta = 30
print(f"{theta=}, {cos(radians(theta))=:.3f}")
