x = 2

try:
    result = 5 / x
except ZeroDivisionError:
    print('division by zero')
    raise  # propagate
except (ValueError, ArithmeticError) as e:
    print(e)
    print(e.__cause__)  # cause for chained exceptions
    print(e.__context__)  # contains additional info when an exception is raised during processing of another exception
    raise AssertionError('Chained Error') from e  # chaining exceptions
else:
    # this block is executed if no exception is thrown
    print(result)
finally:
    # this block is always executed
    print('Done.')
