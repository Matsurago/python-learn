import string

message: str = '  hello world  '
print(message.title())  # capitalize
print(message.upper())  # upper case;  use lower() for lower case
print(message.strip())  # trim;  use rstrip(), lstrip() to remove spaces from right (left)

message += str(123)     # int to str (no implicit conversion)
print(message)

# format strings
message = f"Previous message is: '{message}'."
print(message)

s1 = 'test'
s2 = 'test'
print('s1 == s2:', s1 == s2)   # can compare strings with =

# check for empty string
s1 = ''
if s1:
    print('Not an empty string.')

# When 'r' prefix is present, a character following a backslash is included in the string without change,
# and all backslashes are left in the string
raw_path = r'C:\Program Files\test.txt'
print(raw_path)

# predefined strings
print('Letters:', string.ascii_letters)
print('Lower case:', string.ascii_lowercase)
print('Printable:', string.printable)

sentence = 'I have a cat, which is white'
tokens = sentence.split()  # split by whitespace
print(tokens)
sub_sentences = sentence.split(',')  # split by the given separator (can limit the number of splits with 2nd arg)
print(sub_sentences)
# split into tuple (str_before, separator, str_after)
print('a cat and dog and mouse'.partition('and'))  # use rpartition() for reverse search
# join tokens back
print(','.join(tokens))

# split into lines (includes empty lines)
some_lines = """
    line 1
    line 2
    line 3
"""
print(some_lines.splitlines())

print('Contains only alphanumeric characters:', 'a1b2'.isalnum())
print('Contains only digits:', '123'.isdigit())
print('Contains only letters:', 'a12'.isalpha())
print('Contains only lower-case letters:', 'abc'.islower())
print('Contains only space:', '  \t\n '.isspace())

# char to int
print("Code of 'A' is", ord('A'))

# starts with:
print('hello world'.startswith('hello'))
# ends with:
print('hello world'.endswith('world'))

# count number of occurrences
template_str = 'The %s are known for %s and %s.'
print(f"Count of '%s' in '{template_str}' is:", template_str.count('%s'))
# count number of occurrences starting with index
print(f"Count of '%s' in '{template_str}' (pos >= 10) is:", template_str.count('%s', 10))

# find a substring (-1 if not found)
print(template_str.find('known'))
print(template_str.find('cats'))
# find a substring, searching from the end
print(template_str.rfind('%s'))

# determine if a string is contained within another string
print('known' in template_str)

# replace all
print(template_str.replace('%s', 'cats'))
# replace only given number of occurrences
print(template_str.replace('%s', 'cats', 1))
