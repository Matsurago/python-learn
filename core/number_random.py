import random


# both left and right limits are included
dice = random.randint(1, 6)
assert 1 <= dice <= 6

# random choice
best_fruit = random.choice(['Apple', 'Kiwi', 'Mango', 'Orange', 'Pear', 'Pineapple'])
print(best_fruit)

# random sample (without replacement)
nums = random.sample(range(0, 99), 6)
print(nums)

# random sample with replacement
dice = [random.choice(range(1, 7)) for _ in range(4)]
print(dice)

# can set seed for reproducible experiments
random.seed(32)
