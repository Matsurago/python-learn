import os
from pathlib import Path

# OS-independent path
generators_py = Path('../..', 'core', 'generators.py')
# another way to do the same
functions_py = Path('../..') / 'core' / 'functions' / 'functions.py'
# this also works
exceptions_py = Path('../exceptions.py')

assert generators_py.exists()
assert functions_py.exists()
assert exceptions_py.exists()

# Path object can be used whenever a filename is required:
with open(functions_py) as f:
    print('File head(50): ', f.read(50))

# Path object can be used to easily read or write a file (see also: read_bytes, write_text, write_bytes)
file_contents = functions_py.read_text(encoding='utf-8')
print('File head(50): ', file_contents[:50])

# can get file properties with Path
print('\n= generators.py =')
print('is file:', generators_py.is_file())
print('is dir:', generators_py.is_dir())
print('file size (bytes):', generators_py.stat().st_size)
print()

# home dir
print('Home dir:', Path.home())
# current working directory
print('Script dir:', Path.cwd())
# another way to get current working directory
print(os.path.abspath('../../stdlib'))
# change current working directory
os.chdir(Path('..'))
print(Path.cwd())
