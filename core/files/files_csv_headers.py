import csv
from pathlib import Path


# read data row by row from CSV with headers
# NOTE: can also read CSV without headers, supplying list of headers as a parameter (useful for code readability)
with open(Path('./data/with_headers.csv')) as f:
    reader = csv.DictReader(f)  # csv.DictReader(f, ['header1', 'header2'])
    for row in reader:
        print(row['id'], row['name'], row['surname'], row['age'])

# write data to CSV with headers
with open(Path('./data/output_headers.csv'), 'w') as f:
    writer = csv.DictWriter(f, ['number', 'element', 'bonds'], lineterminator='\n')
    writer.writeheader()
    writer.writerow({'number': 7, 'element': 'N', 'bonds': 4})
    writer.writerow({'number': 8, 'element': 'O', 'bonds': 2})
