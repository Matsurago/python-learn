import configparser
from pathlib import Path

config = configparser.ConfigParser()
config.read(Path('./data/data.ini'))

print('Screen width:', config['screen']['width'])
print('Screen height:', config['screen']['height'])
print('Language:', config['locale']['lang'])
