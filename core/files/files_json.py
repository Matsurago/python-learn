import datetime
import json
from pathlib import Path


data = {"id": 3, "name": "John Miller", "org": "ACME"}

# write JSON to a file
filename = Path("./data/saved_data.json")

with open(filename, "w") as file:
    json.dump(data, file, indent=2)

# read JSON from a file
with open(filename) as file:
    loaded_data = json.load(file)
print(loaded_data)

# write JSON to a string
data_s = json.dumps(data)  # also can format with ident=2
print(data_s)

# read JSON from a string
loaded_data = json.loads(data_s)
print(loaded_data)


# use a custom JSON encoder
class DateEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()
        return super().default(obj)


date = datetime.datetime.now()  # not serializable by default
result = json.dumps(date, cls=DateEncoder)
print(result)

# Option 2: use conversion to string for non-serializable types
result = json.dumps(date, default=str)
print(result)
