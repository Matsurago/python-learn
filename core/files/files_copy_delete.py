import os
import shutil

# copy a file
# can specify either filename or folder as the target
shutil.copy('./files_copy_delete.py', './files_copy.py.backup')
# delete a file
os.unlink('./files_copy.py.backup')

# copy an entire folder
shutil.copytree('.', 'files_backup')
# move folder
shutil.move('./files_backup', './files_bak')
# delete a folder
shutil.rmtree('./files_bak')
