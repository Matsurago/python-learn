from pathlib import Path
from xml.etree.ElementTree import ElementTree, Element, dump

# create XML
# create the root element with attributes
root = Element('employees', company='ACME Inc.', year='2021')
root.append(Element('employee', name='Alice'))
root.append(Element('employee', name='Bob'))

# write XML to stdout
dump(root)

# read XML
doc = ElementTree(file=Path('data/data.xml'))
title_element = doc.find('title')
print('\nTitle:', title_element.text)

description_text = doc.findtext('description')
print('Description:', description_text)

for item_element in doc.findall('items/item'):
    item_id = item_element.get('id')
    item_text = item_element.text
    print(f'id: {item_id}, value: {item_text}')
