import os
from pathlib import Path

filename = Path('data/people.csv')

# read entire file
with open(filename) as file:   # try-with-resources
    data = file.read()
    print(data.rstrip())   # remove the new line at the end

print("---")

# read file line by line
with open(filename, encoding='utf-8') as file:  # can specify encoding when it is different from the system's
    for line in file:
        print(line.rstrip())

# read file into a list of lines
with open(filename) as file:
    lines = file.readlines()

print(len(lines))

# read specific bytes
with open(filename, 'rb') as file:
    file.seek(-5, os.SEEK_END)  # go to 5th byte from the end of the file
    print('Loc:', file.tell())  # current location
    last_bytes = file.read(5)

print('Last bytes:', last_bytes)

# write to a file
# other modes:
# - 'r': read
# - 'a': append
# - 'r+': read and write
# - 'x': write, but only if the file doesn't exist
with open(Path('./data/people_output.csv'), 'w') as file:
    file.write('test,1\n')  # arg should be a string if file is opened in a non-binary mode
    file.write('test,2\n')
    # use print to write to a file
    print('test,3\n', file=file, sep='', end='')

# write to a binary file
text = 'hello world'
with open(Path('./data/binary_output.bin'), 'wb') as file:
    file.write(text.encode('utf-8'))  # arg should be bytes if file is opened in a binary mode

# handle exceptions
try:
    with open(filename) as file:
        contents = file.read()
except FileNotFoundError:
    print('File does not exist.')
except FileExistsError:
    pass   # ignore the exception silently
else:
    # executes when "try" completes successfully
    words = contents.split()
    print('The file contains', len(words), 'word(s).')
