import csv
from pathlib import Path


# read all data at once from CSV
with open(Path('./data/data.csv')) as f:
    reader = csv.reader(f)
    all_data = list(reader)

print(all_data)

# read data row by row from CSV
with open(Path('./data/data.csv')) as f:
    reader = csv.reader(f)
    for row in reader:
        print(reader.line_num, row)

# write data to CSV
with open(Path('./data/output.csv'), 'w') as f:
    writer = csv.writer(f, lineterminator='\n')
    writer.writerow(['a11', 'a12', 'a13'])
    writer.writerow(['a21', 'a22', 'a23'])
