import os
from pathlib import Path

for folder, subfolders, filenames in os.walk(Path('../../algorithms')):
    if Path(folder).name == '__pycache__':
        continue

    print(folder)

    for subfolder in subfolders:
        pass

    for filename in filenames:
        print((' ' * 2) + filename)
