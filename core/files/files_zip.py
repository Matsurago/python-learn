import zipfile
from pathlib import Path
from zipfile import ZipFile

# read and extract
with ZipFile(Path('data/data.zip')) as archive:
    entries = archive.namelist()
    print(entries)

    # extract all compressed files into the speficied folder
    archive.extractall(Path('./data/output'))

# create an archive
with ZipFile(Path('./data/code.zip'), 'w') as archive:
    archive.write('files_zip.py', compress_type=zipfile.ZIP_DEFLATED)
    archive.write('files_json.py', compress_type=zipfile.ZIP_DEFLATED)
