# serialize any Python object to a binary file
import pickle

some_data = [1, 2, {"x": 3, "y": 4}]

# serialize (make sure it is binary mode)
with open("serialized-data.p", "wb") as f:
    pickle.dump(some_data, f)

# deserialize (make sure it is binary mode)
with open("serialized-data.p", "rb") as f:
    loaded_data = pickle.load(f)

print(loaded_data)
