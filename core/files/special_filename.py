# can pass raw bytes to open to create a special filename
if __name__ == '__main__':
    filename = b'\x5c\x2e\x65\x78\x65'  # \.exe (invisible on Windows)

    with open(filename, 'w') as f:
        f.write('test')

    with open(filename, 'r') as f:
        data = f.read()

    print(data)
