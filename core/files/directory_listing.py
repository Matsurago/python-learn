import os
from pathlib import Path

if __name__ == '__main__':
    # list all files and folders in the given folder
    for filename in os.listdir('.'):
        print(filename)

    print('-----')

    # list all files with the given extension
    for filename in Path('.').glob('*.py'):
        print(filename)

    print('-----')

    # list all files with the given extension, including subfolders
    for filename in Path('.').rglob('*.csv'):
        print(filename)  # actually a relative path rather than filename
