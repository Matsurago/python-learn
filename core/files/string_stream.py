# can use Files API with strings
import io

str_data = "Line 1\nLine 2\nLine 3"

# as source
with io.StringIO(str_data) as f:
    lines = f.readlines()

print(lines)

# as destination: create an empty stream and write to it
with io.StringIO() as f:
    f.write("Hello")
    f.write(" World!")
    result = f.getvalue()

print(result)
