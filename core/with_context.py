# can write custom code that works with the 'with' statement to
#   - acquire and release resources
#   - temporary switch context
from contextlib import contextmanager


@contextmanager
def as_admin(reason: str):
    print("temporary raising user privileges, reason:", reason)
    # userService.makeAdmin(user)
    try:
        yield
    finally:
        print("restoring standard user rights")
        # userService.revokeAdminRights(user)


with as_admin('test'):
    print('I am an admin for now')


# can return an object to be available within the 'with' statement

# say, we have a resource that needs to be allocated and cleaned up
# but we also want to work with it within the 'with' statement:
class Resource:
    def __init__(self):
        self.handle = 0

    def allocate(self):
        self.handle = 0x7a
        print('allocating a resource')

    def execute(self):
        print(f'working with a resource {self.handle=}')

    def cleanup(self):
        self.handle = 0
        print('cleaning up')


@contextmanager
def open_resource():
    res = Resource()
    res.allocate()
    try:
        yield res
    finally:
        res.cleanup()


with open_resource() as resource:
    resource.execute()
