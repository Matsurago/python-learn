import asyncio
import random


async def hard_work(param):
    print(f'started task {param}')
    await asyncio.sleep(random.randint(1, 5))
    print(f'finished task {param}')


async def main():
    tasks = []
    for i in range(1, 6):
        coroutine = hard_work(i)  # this doesn't execute the async function yet, returns smth like generator
        tasks.append(coroutine)

    await asyncio.gather(*tasks)  # runs the async functions concurrently


if __name__ == '__main__':
    asyncio.run(main())
