import random
import time
from concurrent.futures import ThreadPoolExecutor


def square(x: int):
    print(f'thread {x} started\n', end='')
    time.sleep(random.randint(1, 5))
    print(f'thread {x} done.\n', end='')
    return x ** 2


if __name__ == '__main__':
    with ThreadPoolExecutor(max_workers=4) as pool:
        futures = [pool.submit(square, i) for i in range(10)]
        print('all tasks submitted.\n', end='')

        sum_squares = 0
        for future in futures:
            sum_squares += future.result()

    print(f'Result = {sum_squares}')
