import subprocess
from pathlib import Path


# open a file with the default app (Windows)
subprocess.Popen(['start', Path('../files/data/data.csv')], shell=True)

# launch an app and pass it some arguments
handle = subprocess.Popen([r'C:\Windows\notepad.exe', 'thread.py'])
handle.wait()  # wait for app to close
