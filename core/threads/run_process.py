import os
import subprocess
import time


# run a command line app and capture output
result = subprocess.run('git branch', capture_output=True, encoding='utf-8')
result.check_returncode()  # throws on non-zero exit
output = result.stdout
print(output)

# easier way to run an app and capture its output
output = subprocess.check_output('git branch')   # may raise an exception
print(output)  # returns bytes, not str

# run a process as from OS terminal
os.system('date /T')  # Windows

# run a command line app async
proc = subprocess.Popen('ping ya.ru')
while proc.poll() is None:
    print('working...')
    time.sleep(1)
print('ping exit status:', proc.poll())

# run many apps in parallel async
n = 10
processes = [subprocess.Popen('ping -n 1 ya.ru') for _ in range(n)]
for process in processes:
    process.communicate(timeout=5)  # wait for all to finish, can specify timeout to avoid hanging processes

# run a command and send it data
data = os.urandom(32)
env_copy = os.environ.copy()
env_copy['password'] = '123456'

proc = subprocess.Popen('openssl enc -pbkdf2 -pass env:password',
                        env=env_copy, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
proc.stdin.write(data)  # send data
proc.stdin.flush()
out, err = proc.communicate()
print('encrypted:', out)  # output
