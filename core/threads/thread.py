import time
from threading import Thread


def pause(seconds: int):
    print(f'Pausing for {seconds} second(s)...')
    for i in range(seconds, 0, -1):
        print(f'{i}...')
        time.sleep(1)
    print('Done.')


if __name__ == '__main__':
    t = Thread(target=pause, args=[3])
    t.start()
    t.join()
