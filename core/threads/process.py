from multiprocessing import Process
import os


def info(name):
    pid = os.getpid()
    print(f'process {name}, id = {pid}')


if __name__ == '__main__':
    processes = [Process(target=info, args=(i + 1, )) for i in range(3)]

    for process in processes:
        process.start()

    for process in processes:
        process.join()

    print('finish.')
