# queue is used to coordinate work between threads
import time
from queue import Queue
from threading import Thread


def consumer(my_queue: Queue):
    while True:
        data = my_queue.get()  # blocking
        time.sleep(1)
        print(f'consumer processed: {data}, queue size: {q.qsize()}')
        my_queue.task_done()

        if data == '#quit':
            print('shutting down the consumer...')
            time.sleep(1)
            break


if __name__ == '__main__':
    q = Queue(2)  # max buffer size
    thread = Thread(target=consumer, args=(q, ))
    thread.start()

    print('supplying data...')
    q.put('a')  # blocks if max number of buffered items is reached
    q.put('b')
    q.put('c')
    q.put('#quit')

    q.join()  # wait until task_done() is called on all items that were put in the queue

    thread.join()
    print('all done.')
