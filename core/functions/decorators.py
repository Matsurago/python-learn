# decorators are means to do AOP in Python
from functools import wraps


def trace(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        print(f"Calling {f.__name__} with args: {args!r} {kwargs!r}")
        result = f(*args, **kwargs)
        print(f"  Result: {result}")
        return result
    return wrapper


@trace
def fibo(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fibo(n - 1) + fibo(n - 2)


res = fibo(4)
print(f"Fibo(4) = {res}")
