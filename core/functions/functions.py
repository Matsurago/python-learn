# note: never pass a mutable object such as List, Set, Dict, or a class instance
# as a default value, because it is initialized only once, and will be reused
# each time the function is called
def hello(name, surname, ending="!"):
    """Greets a user """    # docstring
    return f"Hello, {name} {surname}{ending}"


greeting = hello(surname="Milkovich", name="Julia")
print(greeting)


# a function with positional varargs
# passed args are packed into a tuple (not a list)
def varargs(*args):
    print(args)


varargs(1, 2, 3, 4)
# use star operator to unpack a list into a vararg argument for the function:
data = [5, 6, 7]
varargs(*data)


# a function with named varargs
def var_named(**args):
    for key, value in args.items():
        print(key, "->", value)


var_named(n=10, repeat=False, mode="accurate")
# use double-star operator to unpack a dictionary into named varargs:
values = {'alpha': .3, 'beta': .6}
var_named(**values)


# a function with only-positional and only-keyword arguments
# a, b:  positional only
# precision:  can be either positional or keyword
# spacing, alignment:  keyword only
def fmt_mul(a, b, /, precision, *, spacing, alignment):
    a_fmt = f'{a:{alignment}{spacing}.{precision}f}'
    b_fmt = f'{b:{alignment}{spacing}.{precision}f}'
    r_fmt = f'{(a * b):{alignment}{spacing}.{precision}f}'
    return f'{a_fmt} x {b_fmt} = {r_fmt}'


result = fmt_mul(6.789, 2.8723, 1, spacing=5, alignment='^')
print(result)
