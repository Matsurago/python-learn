import operator
from functools import partial, reduce


# filter <use list comprehensions instead>
cubes = [n ** 3 for n in range(5)]
filtered_numbers = list(filter(lambda x: x > 24, cubes))
assert filtered_numbers == [27, 64]

# map to string <use list comprehensions instead>
cubes_str = list(map(str, cubes))
print(cubes_str)

# map and filter combination <use list comprehensions instead>
pow_of_two = partial(operator.pow, 2)
gt_eight = partial(operator.le, 9)
result = list(filter(gt_eight, map(pow_of_two, range(1, 6))))
print(result)

# reduce
values = [1, 3, 7, 13, 17]
product = reduce(operator.mul, values, 1)
assert product == 4641
