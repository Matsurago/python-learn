import atexit


def cleanup():
    print('Cleaning up allocated resources...')


atexit.register(cleanup)

print('Allocating resources...')
