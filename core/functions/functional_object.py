# Tip: use functional objects instead of stateful closures
class Multiplier:
    def __init__(self):
        self.history = []

    def __call__(self, a, b):
        result = a * b
        self.history.append(result)
        return result


multi = Multiplier()

print(multi(2, 5))
print(multi(17, 11))

print(multi.history)
