# import Pizza as p          // give module an alias
# from Module import *       // import all functions
from core.functions.functions import varargs as f, var_named as g

# equivalent to: import itertools
# this is VERY USEFUL to import modules, names of which are not known in advance
itertools = __import__("itertools")

f(1)
g(arg=2)
