# any
numbers = [1, 3, 5, 7, 9]
has_even = any(x % 2 == 0 for x in numbers)
assert (not has_even)

# all
all_positive = all(x > 0 for x in numbers)
assert all_positive

# zip
symbols = ['msft', 'aapl', 'goog']
stock_price = [280, 155, 2600]
data = list(zip(symbols, stock_price))  # lazy, need to cast to list
assert data == [('msft', 280), ('aapl', 155), ('goog', 2600)]

# another useful example of zip
csv = 'MSFT,1000,280.4'
types = [str, int, float]
parsed = [type_(token) for token, type_ in zip(csv.split(','), types)]
assert parsed == ['MSFT', 1000, 280.4]

