import os


# generator expressions (lazy evaluation)
for n in (n ** 3 for n in range(0, 30) if n < 8):
    print(n, end=' ')
print()


# generator returns a lazy iterator
def list_files(parent: str):
    for filename in os.listdir(parent):
        yield os.path.join(parent, filename)


# executes immediately (a lazy operation)
my_files = list_files('.')
print(next(my_files))   # first file
print(next(my_files))   # second file


# use 'yield from' to combine generators
def combined_gen():
    yield 'test'
    yield from (c for c in 'abc')
    yield from list_files('threads')


print(list(combined_gen()))
