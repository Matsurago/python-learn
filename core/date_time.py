import locale
import time
from datetime import date, datetime, timedelta, timezone

# current date (year/month/day)
current_day = date.today()
print(current_day)

# current date & time
current_date = datetime.now()
print(current_date)

# get separately date and time from datetime object
print('Date:', current_date.date(), ', Time:', current_date.time())

# create a date
event_date = datetime(2021, 12, 1, 12, 0, 0)
# specify that the date is in a specific timezone <for timezones besides UTC, install 'pytz' community module>
event_date_utc = event_date.replace(tzinfo=timezone.utc)
# convert to the local timezone
event_date_local = event_date_utc.astimezone()  # gives 12+9 = 21:00 JST
print(event_date_local)

# parse a date
date_format = '%Y-%m-%d %H:%M:%S'
another_date = datetime.strptime('2019-06-30 09:17:35', date_format)
print(another_date)

# format a date
print('Formatted date:', datetime.strftime(event_date, date_format))

# format a date in ISO format, e.g.: 2019-06-30T09:17:35
print('Formatted date (ISO):', another_date.isoformat())

# use timedelta to store duration
delta = timedelta(days=7, hours=12, minutes=30)
print('Delta:', delta)  # human-readable duration
print('Total duration (s):', delta.total_seconds())

# can perform calculations with datetime and timedelta:
future_date = current_date + delta
print(f'{delta.days} days from now is:', future_date)

# compare dates
date_1 = date(2020, 1, 1)
date_2 = date(2020, 2, 12)
assert date_1 < date_2
assert not date_1 > date_2

# convert to UNIX time
unix_ts = time.mktime(current_date.timetuple())
print(unix_ts)

# UNIX time: now
now = time.time()
print('Now (UNIX):', now)

# format UNIX time
print('Now (UNIX, formatted):', time.ctime(now))

# extract date/time values from UNIX time (local timezone)
now_ts_local = time.localtime(now)  # don't pass the argument for current date/time
print(now_ts_local)

# extract date/time values from UNIX time (UTC)
now_ts = time.gmtime(now)  # don't pass the argument for current date/time
print(now_ts)
