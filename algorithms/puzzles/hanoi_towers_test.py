from .hanoi_towers import hanoi_towers


def test_height_one():
    assert hanoi_towers(1, 1, 2) == [(1, 2)]


def test_height_two():
    assert hanoi_towers(2, 1, 2) == [(1, 3), (1, 2), (3, 2)]


def test_height_three():
    expected = [(1, 2), (1, 3), (2, 3), (1, 2), (3, 1), (3, 2), (1, 2)]
    assert hanoi_towers(3, 1, 2) == expected
