def hanoi_towers(height, move_from, move_to):
    if height == 1:
        return [(move_from, move_to)]

    move_temp = (6 - move_from - move_to)

    return hanoi_towers(height - 1, move_from, move_temp) \
        + [(move_from, move_to)] \
        + hanoi_towers(height - 1, move_temp, move_to)
