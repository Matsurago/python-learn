import copy


class Grid:
    EMPTY = '_'
    ALIVE = '*'

    def __init__(self, width, height):
        self.n = width
        self.m = height
        self.cells = [[Grid.EMPTY] * height for _ in range(width)]

    def __getitem__(self, item):
        i, j = item
        return self.cells[i % self.n][j % self.m]

    def __setitem__(self, item, value):
        i, j = item
        self.cells[i % self.n][j % self.m] = value

    def set(self, i, j):
        self[i, j] = Grid.ALIVE

    def unset(self, i, j):
        self[i, j] = Grid.EMPTY

    def alive_neighbours(self, x, y):
        num = 0
        for i in (-1, 0, 1):
            for j in (-1, 0, 1):
                if self[x + i, y + j] == Grid.ALIVE:
                    if i != 0 or j != 0:
                        num += 1
        return num

    def next_state(self, i, j):
        current_state = self[i, j]
        num_alive = self.alive_neighbours(i, j)

        if current_state == Grid.ALIVE:
            if num_alive != 2 and num_alive != 3:
                return Grid.EMPTY
        elif current_state == Grid.EMPTY:
            if num_alive == 3:
                return Grid.ALIVE
        return current_state

    def advance(self):
        grid = copy.copy(self)
        for i in range(grid.n):
            for j in range(grid.m):
                self[i, j] = grid.next_state(i, j)

    def __copy__(self):
        grid = Grid(self.n, self.m)
        grid.cells = copy.deepcopy(self.cells)
        return grid

    def __str__(self):
        s = ''
        for i in range(self.n):
            for j in range(self.m):
                s += self[i, j]
            s += '\n'
        return s


if __name__ == '__main__':
    g = Grid(5, 9)
    g.set(0, 3)
    g.set(1, 4)
    g.set(2, 2)
    g.set(2, 3)
    g.set(2, 4)
    print(g)

    for _ in range(4):
        g.advance()
        print(g)
