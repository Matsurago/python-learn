from .stable_matching import stable_matching


def test_stable_matching_no_input():
    assert stable_matching([], []) == []


def test_stable_matching_trivial_case():
    assert stable_matching([[1]], [[1]]) == [1]


def test_stable_matching_two_pairs_mesh_no_conflict():
    men_prefs = [[2, 1], [1, 2]]
    women_prefs = [[2, 1], [1, 2]]

    assert stable_matching(men_prefs, women_prefs) == [2, 1]


def test_stable_matching_two_pairs_men_mesh_women_conflict():
    men_prefs = [[1, 2], [2, 1]]
    women_prefs = [[2, 1], [1, 2]]

    assert stable_matching(men_prefs, women_prefs) == [1, 2]


def test_stable_matching_two_pairs_conflict():
    men_prefs = [[1, 2], [1, 2]]
    women_prefs = [[2, 1], [1, 2]]

    assert stable_matching(men_prefs, women_prefs) == [2, 1]


def test_stable_matching_three_pairs():
    men_prefs = [[1, 2, 3], [2, 1, 3], [1, 2, 3]]
    women_prefs = [[2, 1, 3], [1, 2, 3], [1, 2, 3]]

    assert stable_matching(men_prefs, women_prefs) == [1, 2, 3]


def test_stable_matching_five_pairs():
    men_prefs = [[3, 1, 4, 2, 5], [2, 3, 1, 4, 5], [1, 3, 4, 5, 2], [1, 2, 4, 3, 5], [4, 1, 5, 2, 3]]
    women_prefs = [[2, 5, 3, 4, 1], [4, 2, 1, 5, 3], [1, 4, 2, 3, 5], [3, 2, 4, 5, 1], [2, 3, 4, 5, 1]]

    assert stable_matching(men_prefs, women_prefs) == [1, 2, 5, 3, 4]
