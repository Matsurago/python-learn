# Breadth First Search (shortest path on unweighted graph)
from collections import deque
from typing import Dict, TypeVar, List

T = TypeVar('T')


def bfs(g: Dict[T, List[T]], src: T) -> Dict[T, int]:
    inf = (1 << 32) - 1

    cost = {v: inf for v in g.keys()}
    cost[src] = 0
    q = deque([src])

    while q:
        v = q.popleft()
        new_cost = cost[v] + 1

        for neighbour in g[v]:
            if cost[neighbour] == inf:
                cost[neighbour] = new_cost
                q.append(neighbour)
    return cost


if __name__ == '__main__':
    graph = {
        'A': ['X', 'B', 'D'],
        'B': ['X', 'A', 'D', 'H'],
        'C': ['X', 'L'],
        'D': ['A', 'B', 'F'],
        'E': ['X'],
        'F': ['D', 'H'],
        'G': ['H', 'Y'],
        'H': ['B', 'F', 'G'],
        'I': ['L', 'J', 'K'],
        'J': ['I', 'L'],
        'K': ['I', 'Y'],
        'L': ['C', 'I', 'J'],
        'X': ['A', 'B', 'C', 'E'],
        'Y': ['G', 'K']
    }

    costs = bfs(graph, 'X')
    print(costs)
    print('min path (X, Y):', costs['Y'])
