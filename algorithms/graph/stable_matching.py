from typing import List


def make_matrix(n: int, m: int, initial_value=0) -> List[List[int]]:
    return [[initial_value] * m for _ in range(n)]


def stable_matching(men_preferences, women_preferences):
    n = len(men_preferences)

    # rearrange men preferences so women come in preference order
    men_preferences = sort_rows_by_value(men_preferences)

    men_pair = [-1] * n
    women_pair = [-1] * n

    free_men = list(range(0, n))
    proposed = make_matrix(n, n, False)

    while free_men:
        m = free_men.pop(0)
        engaged = False

        for i in range(0, n):
            if proposed[m][i]:
                continue

            w = men_preferences[m][i]

            if women_pair[w] == -1:  # if woman w is free
                engaged = True
            else:
                w_current_partner = women_pair[w]
                if women_preferences[w][m] < women_preferences[w][w_current_partner]:
                    engaged = True
                    free_men.append(w_current_partner)

            if engaged:
                men_pair[m] = w
                women_pair[w] = m
                break

    # adjust output
    men_pair = [x + 1 for x in men_pair]
    return men_pair


def sort_rows_by_value(matrix):
    n = len(matrix)
    result = make_matrix(n, n, 0)

    for i in range(0, n):
        for (j, value) in enumerate(matrix[i]):
            result[i][value - 1] = j
    return result
