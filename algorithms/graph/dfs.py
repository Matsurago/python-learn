# Depth First Search
# - can determine if a graph has cycles
from typing import Dict, TypeVar, List

T = TypeVar('T')


def dfs(g: Dict[T, List[T]]) -> (Dict[T, int], Dict[T, int]):
    visited_time = {v: 0 for v in g.keys()}  # first time visited this node (timestamp)
    backtrack_time = {v: 0 for v in g.keys()}  # no more unexplored paths from this node (timestamp)
    t = 0  # time (ticks)

    def dfs_visit(v: T):
        nonlocal g, visited_time, t

        t += 1
        visited_time[v] = t

        for neighbor in g[v]:
            if visited_time[neighbor] == 0:  # unvisited neighbor
                dfs_visit(neighbor)

        t += 1
        backtrack_time[v] = t

    for v in g.keys():
        if visited_time[v] == 0:
            dfs_visit(v)

    return visited_time, backtrack_time


if __name__ == '__main__':
    graph = {
        'A': ['X', 'B', 'D'],
        'B': ['X', 'A', 'D', 'H'],
        'C': ['X', 'L'],
        'D': ['A', 'B', 'F'],
        'E': ['X'],
        'F': ['D', 'H'],
        'G': ['H', 'Y'],
        'H': ['B', 'F', 'G'],
        'I': ['L', 'J', 'K'],
        'J': ['I', 'L'],
        'K': ['I', 'Y'],
        'L': ['C', 'I', 'J'],
        'X': ['A', 'B', 'C', 'E'],
        'Y': ['G', 'K']
    }

    _visit, _backtrack = dfs(graph)

    # sort by time
    print(sorted(_visit.items(), key=lambda x: x[1]))
    print(sorted(_backtrack.items(), key=lambda x: x[1]))
