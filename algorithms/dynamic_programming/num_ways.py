from typing import Set, List


def num_ways(n: int) -> int:
    """
    [S . . . . . . N . . ]
    Starting from cell 0, can move either 1 or 2 steps at a time.
    What is the number of ways to finish on the cell N?
    """
    ways = [0] * (n + 1)
    ways[0] = 1  # starting cell
    ways[1] = 1

    for i in range(2, n + 1):
        ways[i] = ways[i - 2] + ways[i - 1]

    return ways[n]


def num_ways_forbidden_moves(n: int, forbidden: Set[int]) -> int:
    """
    [S . . x . . x N . . ]
    Starting from cell 0, can move either 1 or 2 steps at a time.
    Some cells are forbidden to step onto.
    What is the number of ways to finish on the cell N?
    """
    ways = [0] * (n + 1)
    ways[0] = 1
    ways[1] = 1 if 1 not in forbidden else 0

    for i in range(2, n + 1):
        if i not in forbidden:
            ways[i] = ways[i - 2] + ways[i - 1]

    return ways[n]


def num_ways_min_cost(n: int, cost: List[int]) -> int:
    """
    [S . . x . . x N . . ]
    Starting from cell 0, can move either 1 or 2 steps at a time.
    All cells have an attached cost.
    What is the minimum cost to finish on the cell N?
    """
    min_cost = [0] * (n + 1)
    min_cost[0] = cost[0]
    min_cost[1] = cost[0] + cost[1]

    for i in range(2, n + 1):
        min_cost[i] = cost[i] + min(min_cost[i - 2], min_cost[i - 1])

    return min_cost[n]


def num_ways_2d(n: int, m: int):
    """
    S . . . .
    . . . . .
    . . . . T
    Starting from cell (1, 1), can move either down or right.
    What is the number of ways to reach the cell T = (n, m) ?
    """
    ways = [[0] * (m + 1) for _ in range(n + 1)]  # for convenience init 0-th row and 0-th column with zeros
    ways[1][1] = 1  # starting cell
    for i in range(1, n + 1):
        for j in range(1, m + 1):
            if i == 1 and j == 1:
                continue
            ways[i][j] = ways[i][j-1] + ways[i-1][j]

    return ways[n][m]
