from .num_ways import *


def test_num_ways():
    assert num_ways(1) == 1
    assert num_ways(2) == 2
    assert num_ways(3) == 3
    assert num_ways(4) == 5
    assert num_ways(5) == 8


def test_num_ways_forbidden_moves():
    assert num_ways_forbidden_moves(7, {3, 6}) == 2


def test_num_ways_min_cost():
    assert num_ways_min_cost(5, [1, 7, 2, 4, 8, 1]) == 8


def test_num_ways_2d():
    assert num_ways_2d(1, 1) == 1
    assert num_ways_2d(4, 5) == 35
