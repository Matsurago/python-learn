from typing import TypeVar, List

T = TypeVar('T')


def largest_common_subsequence(a: List[T], b: List[T]) -> int:
    """
    A subsequence S' is a selection of elements from the original
    sequence S in the same order, such that some elements may be
    omitted, for example:
    S  = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ...]
    S' = [1, 2, 5, 13, 34, ...]
    """
    n = len(a)
    m = len(b)

    lcs = [[0] * (m + 1) for _ in range(0, n + 1)]

    for i in range(1, n + 1):
        for j in range(1, m + 1):
            if a[i-1] == b[j-1]:
                lcs[i][j] = 1 + lcs[i-1][j-1]  # the last item is in our max subsequence
            else:
                lcs[i][j] = max(lcs[i-1][j], lcs[i][j-1])

    return lcs[n][m]
