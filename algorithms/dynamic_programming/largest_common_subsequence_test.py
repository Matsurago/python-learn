from .largest_common_subsequence import largest_common_subsequence as lcs


def test_empty():
    assert lcs([], []) == 0


def test_singleton():
    assert lcs([1], [2]) == 0
    assert lcs([1], [1]) == 1


def test_prefix():
    assert lcs([1, 2, 3, 4, 5, 6], [3, 4, 5, 6, 7, 8]) == 4
    assert lcs([1, 3, 5, 7], [1, 5, 7, 12, 15]) == 3


def test_infix():
    assert lcs([0, 2, 4, 6, 8, 10, 12], [-1, 1, 3, 2, 5, 4, 7, 8, 11, 12, 15]) == 4
