def newton(f, df, x0):
    eps = 1e-9
    x, xn = x0, x0

    while abs(f(xn)) > eps:
        xn = x - f(x) / df(x)
        x = xn

    return xn
