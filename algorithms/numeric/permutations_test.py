from .permutations import all_perm, all_perm_with_repetitions


def test_empty_with_rep():
    result = list(all_perm_with_repetitions([]))
    assert result == [[]]


def test_single_with_rep():
    result = list(all_perm_with_repetitions([1]))
    assert result == [[1]]


def test_two_ints_with_rep():
    expected = [[1, 1], [1, 2], [2, 1], [2, 2]]

    result = list(all_perm_with_repetitions([1, 2]))
    assert result == expected


def test_three_chars_with_rep():
    expected = ['aaa', 'aab', 'aac', 'aba', 'abb', 'abc', 'aca', 'acb', 'acc',
                'baa', 'bab', 'bac', 'bba', 'bbb', 'bbc', 'bca', 'bcb', 'bcc',
                'caa', 'cab', 'cac', 'cba', 'cbb', 'cbc', 'cca', 'ccb', 'ccc']

    result = list(all_perm_with_repetitions(list('abc')))
    result = [''.join(res) for res in result]

    assert result == expected


def test_five_numbers_with_rep():
    result = list(all_perm_with_repetitions(range(1, 6)))
    assert len(result) == 5 ** 5


def test_empty():
    result = list(all_perm([]))
    assert result == [[]]


def test_single():
    result = list(all_perm([1]))
    assert result == [[1]]


def test_two_ints():
    expected = [[1, 2], [2, 1]]

    result = list(all_perm([1, 2]))
    assert result == expected


def test_three_chars():
    expected = ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']

    result = list(all_perm(list('abc')))
    result = [''.join(res) for res in result]

    assert result == expected


def test_five_numbers():
    result = list(all_perm(range(1, 6)))
    assert len(result) == 120
