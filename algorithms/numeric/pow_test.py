from .pow import power


def test_powers_of_two():
    assert power(2, 0) == 1
    assert power(2, 1) == 2
    assert power(2, 2) == 4
    assert power(2, 3) == 8
    assert power(2, 4) == 16
    assert power(2, 5) == 32
    assert power(2, 6) == 64
    assert power(2, 7) == 128
    assert power(2, 8) == 256
    assert power(2, 9) == 512
    assert power(2, 10) == 1024


def test_powers_of_three():
    assert power(3, 2) == 9
    assert power(3, 3) == 27
    assert power(3, 4) == 81
    assert power(3, 5) == 243
    assert power(3, 12) == 531441


def test_power_of_big_num():
    assert power(65, 17) == 6599743590836592050933837890625
