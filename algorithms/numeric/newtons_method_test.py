import pytest
from math import exp
from .newtons_method import newton


def test_solve_quadratic():
    result = newton(lambda x: x**2-4, lambda x: 2*x, 10)
    assert result == pytest.approx(2.000)


def test_solve_eq1():
    result = newton(lambda x: -4*x + 6*exp(13-x) + 161,
                    lambda x: -4 - 6*exp(13-x),
                    13)
    assert result == pytest.approx(40.250)
