from typing import Generator, TypeVar, Sequence

T = TypeVar('T')


def all_perm_with_repetitions(array: Sequence[T], prefix: Sequence[T] = None) -> Generator[Sequence[T], None, None]:
    prefix = prefix or []

    if len(prefix) == len(array):
        yield prefix
        return

    for _, value in enumerate(array):
        yield from all_perm_with_repetitions(array, prefix + [value])


def all_perm(array: Sequence[T], prefix: Sequence[T] = None) -> Generator[Sequence[T], None, None]:
    prefix = prefix or []

    if len(prefix) == len(array):
        yield prefix
        return

    for _, value in enumerate(array):
        if value not in prefix:
            yield from all_perm(array, prefix + [value])
