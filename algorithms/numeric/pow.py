def power(a: float, n: int) -> float:
    """
    Raises a number to the given power.
    Complexity: O(log n)
    """
    if n == 0:
        return 1

    r = a
    acc = 1  # an accumulator for non-even power values

    while n > 1:
        if n % 2 != 0:
            acc *= r
        r *= r
        n //= 2

    return r * acc
