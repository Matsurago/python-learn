from collections import defaultdict
from typing import Dict, List, TypeVar, Generic, Set

T = TypeVar('T')


class Variable(Generic[T]):
    def __init__(self, name: str, index: int, domain: List[T]):
        self.name = name
        self.index = index
        self.domain = domain

    def __eq__(self, other):
        return self.index == other.index

    def __lt__(self, other):
        return self.index < other.index

    def __hash__(self):
        return hash(self.index)

    def __str__(self):
        return f'{self.name}{self.index + 1}'

    def __repr__(self):
        return f'{self.name}{self.index + 1}'


class IntVariable(Variable):
    def __init__(self, name: str, index: int, start: int, end: int):
        super().__init__(
            name=name,
            index=index,
            domain=list(range(start, end + 1))
        )


class ConstraintStore:
    def __init__(self):
        self.space: Dict[Variable[T], List[T]] = {}  # search space
        self.variables: List[Variable] = []  # variables
        self.constraint = None  # constraint function

        self.backlog = []  # intermediate states stored for backtracking
        self.assigned_variables: Set[Variable] = set()  # assigned variables
        self.choices: Dict[Variable[T], T] = {}
        self.next = defaultdict(int)

    def add_int_variable(self, var_name: str, start: int, end: int):
        var_id = len(self.variables)
        self.add_variable(IntVariable(var_name, var_id, start, end))

    def add_int_array(self, size: int, var_name: str, start: int, end: int):
        for i in range(size):
            self.add_int_variable(var_name, start, end)

    def add_variable(self, var: Variable):
        self.variables.append(var)
        self.space[var] = var.domain

    def set_constraint(self, constraint):
        self.constraint = constraint

    def set_variable(self, var: Variable[T], var_value: T):
        self.backlog.append((self.space.copy(), var))
        self.assigned_variables.add(var)
        self.choices[var] = var_value

        print(f'[*] set {var} = {var_value}')
        self.propagate(var, var_value)

    def propagate(self, fixed_var, fixed_value):
        new_space = defaultdict(list)

        for var in self.variables:
            if var.index != fixed_var.index:
                new_space[var] = self.satisfy(fixed_value, var, fixed_var.index, var.index)
            else:
                new_space[fixed_var] = [fixed_value]

        self.space = new_space

    def satisfy(self, fixed_var, var2, fixed_index, j):
        values = []

        for xi_val in self.space[var2]:
            if self.constraint(fixed_var, xi_val, fixed_index, j):
                values.append(xi_val)

        return values

    def find_min_range_var(self):
        available_variables = [v for v in self.variables if v not in self.assigned_variables]

        m = len(self.space[available_variables[0]])
        m_var = available_variables[0]

        for var in available_variables:
            if len(self.space[var]) < m:
                m = len(self.space[var])
                m_var = var

        return m_var

    def backtrack(self):
        while True:
            space, var = self.backlog.pop()
            self.assigned_variables.remove(var)
            del self.choices[var]

            if self.next_choice(var) < len(space[var]):
                print(f'[*] backtracked to assignment of {var}')
                self.space = space
                print(self)
                break

    def solve(self):
        print('[*] starting search')
        print('[*] initial state:')
        print(self)

        while len(self.assigned_variables) != len(self.variables):
            m_var = self.find_min_range_var()
            m_domain = self.space[m_var]
            if not m_domain:
                self.backtrack()
                m_var = self.find_min_range_var()
                m_domain = self.space[m_var]

            m_value = m_domain[self.do_next_choice(m_var)]
            self.set_variable(m_var, m_value)

            print(self)

        if len(self.assigned_variables) == len(self.variables):
            print('[*] found solution:', self.choices)
        else:
            print('[*] no solution')

    def do_next_choice(self, var) -> int:
        key = (tuple(sorted(self.choices.items())), var)
        choice = self.next[key]
        self.next[key] += 1
        return choice

    def next_choice(self, var) -> int:
        key = (tuple(sorted(self.choices.items())), var)
        return self.next[key]

    def choices_str(self):
        return ', '.join(f'{key} = {value}' for key, value in sorted(self.choices.items()))

    def __str__(self):
        s = '  choices:\n    ' + self.choices_str() + '\n  search space:\n'
        for var, var_domain in self.space.items():
            if var not in self.assigned_variables:
                s += f'    {var}: {var_domain}\n'

        if len(self.assigned_variables) == len(self.variables):
            s += '    <empty>\n'
        return s


def queen_constraint(xi_val, xj_val, i, j):
    return not (xi_val == xj_val or xi_val + (j - i) == xj_val or xi_val - (j - i) == xj_val)


store = ConstraintStore()
store.add_int_array(size=8, var_name='X', start=1, end=8)

store.set_constraint(queen_constraint)

print(store)

store.solve()
