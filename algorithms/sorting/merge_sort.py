def merge_sort(a):
    """
    Merge Sort
    Worst/Best/Average:  Θ(n * lg n)
    """
    merge_sort_(a, 0, len(a))


def merge_sort_(a, l, r):
    n = r - l

    if n <= 1:
        return

    mid = l + n // 2
    merge_sort_(a, l, mid)
    merge_sort_(a, mid, r)

    l_size, r_size = mid - l, r - mid

    # copy temporarily into left and right decks,
    # because values in 'a' will be overwritten
    left = [0] * l_size
    for i in range(l_size):
        left[i] = a[l + i]

    right = [0] * r_size
    for i in range(r_size):
        right[i] = a[mid + i]

    i, j, k = 0, 0, l

    while i < l_size and j < r_size:
        if left[i] <= right[j]:
            a[k] = left[i]
            i += 1
        else:
            a[k] = right[j]
            j += 1
        k += 1

    # copy remaining elements from a deck
    while i < l_size:
        a[k] = left[i]
        i += 1
        k += 1
    while j < r_size:
        a[k] = right[j]
        j += 1
        k += 1
