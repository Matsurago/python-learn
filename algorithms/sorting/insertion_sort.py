def insertion_sort(a):
    """
    Invariant: at the end of i-th iteration, first i elements are the same
               as in the original array, but are sorted.
    Worst Case: Θ(n^2), when array is in reversed sorted order
    Best Case:  Θ(n),   when array is already sorted
    Average:    O(n^2)
    """
    for i in range(1, len(a)):
        cur = a[i]
        pos = i

        while pos > 0 and a[pos-1] > cur:
            a[pos] = a[pos-1]
            pos -= 1

        a[pos] = cur
