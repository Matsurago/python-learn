from random import randint, seed
from typing import List


def random_list(size: int, breadth: int) -> List[int]:
    seed()
    return [randint(-breadth, breadth) for _ in range(0, size)]


def run_test_sort(sorting_algorithm):
    trials = 100
    max_list_size = 1000
    max_value_breadth = 10_000

    for trial in range(0, trials):
        array = random_list(randint(0, max_list_size), randint(0, max_value_breadth))

        a = array.copy()
        sorting_algorithm(a)

        assert sorted(array) == a

        print(f"{trial+1}/{trials}")
