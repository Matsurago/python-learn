from .bubble_sort import bubble_sort
from .sort_test_utils import run_test_sort


def test_empty():
    a = []
    bubble_sort(a)
    assert a == []


def test_singleton():
    a = [1]
    bubble_sort(a)
    assert a == [1]


def test_double():
    a = [2, 1]
    bubble_sort(a)
    assert a == [1, 2]


def test_list10():
    a = [8, 7, 1, 2, 9, 5, 4, 3, 6]
    bubble_sort(a)
    assert a == list(range(1, 10))


def test_reversed():
    a = [5, 4, 3, 2, 1]
    bubble_sort(a)
    assert a == list(range(1, 6))


def test_random_list():
    run_test_sort(bubble_sort)
