def bubble_sort(a):
    """
    Invariant: at the end of i-th iteration, last i elements are sorted
    """
    n = len(a)

    for i in range(0, n-1):
        for j in range(1, n-i):
            if a[j-1] > a[j]:
                a[j-1], a[j] = a[j], a[j-1]
