from data_structures.heap import Heap


def heap_sort(a):
    heap = Heap.from_list(a)

    for i in range(len(a) - 1, -1, -1):
        heap[0], heap[i] = heap[i], heap[0]  # put max element the last
        heap.n -= 1
        heap.heapify(0)  # fix the heap property

    a[:] = heap.array
