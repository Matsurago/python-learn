def selection_sort(a):
    """
    Invariant: at the end of i-th iteration, first i elements are sorted.
    Worst Case: O(n^2)
    Best Case:  O(n^2)
    Average:    O(n^2)
    """
    n = len(a)

    for i in range(0, n-1):
        cur_min = a[i]
        min_pos = i

        for j in range(i+1, n):
            if a[j] < cur_min:
                cur_min = a[j]
                min_pos = j

        a[i], a[min_pos] = a[min_pos], a[i]
