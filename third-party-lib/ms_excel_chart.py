from pathlib import Path

import openpyxl
from openpyxl.chart import Reference, BarChart


wb = openpyxl.Workbook()
sheet = wb.active

# put some data
for i in range(1, 11):
    sheet.cell(i, 1).value = i

# create a chart
chart = BarChart()
chart.title = 'Chart 1'

# add data to the chart
chart_data_range = Reference(sheet, min_col=1, min_row=1, max_col=1, max_row=10)  # A1-A10
chart.add_data(chart_data_range)

# add the chart to the Worksheet
sheet.add_chart(chart, 'C3')

wb.save(Path('./data/chart.xlsx'))
