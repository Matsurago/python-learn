from pathlib import Path
import openpyxl


# load a workbook
wb = openpyxl.load_workbook(Path('./data/sheet.xlsx'))
print(wb.sheetnames)

# get a sheet from the workbook
sheet = wb['Sheet1']

# sheet size
print(f'Sheet1 size: {sheet.max_row}x{sheet.max_column}')

# get a cell
a1 = sheet['A1']
print(f"Cell {a1.coordinate} ({a1.row}, {a1.column}) has value '{a1.value}'")

# get a cell by coordinates
b1 = sheet.cell(1, 2)
print(f"Cell {b1.coordinate} ({b1.row}, {b1.column}) has value '{b1.value}'")

# get a range of cells
for row in sheet['A1:B3']:
    for cell in row:
        print(cell.value, end=' | ')
    print()
