import pyautogui


screen_size = pyautogui.size()
print(f'Screen size: {screen_size.width}x{screen_size.height}')

# move mouse to absolute coordinates
# pyautogui.moveTo(screen_size.width // 2, screen_size.height // 2, duration=0.25)

# move mouse to coordinates relative to current position
# pyautogui.move(100, 0, duration=0.25)
# pyautogui.move(0, 100, duration=0.25)
# pyautogui.move(-100, 0, duration=0.25)
# pyautogui.move(0, -100, duration=0.25)

# get current position
pos = pyautogui.position()
print(pos)

# right click mouse at coordinates
pyautogui.rightClick(500, 500)
# double left click at coordinates
pyautogui.doubleClick(1000, 300)
# scroll
pyautogui.scroll(300)

# get list of windows
all_windows = pyautogui.getAllWindows()
print(all_windows)

active_window = pyautogui.getActiveWindow()
print(active_window)

# use a hotkey
pyautogui.hotkey('ctrl', 's')

# write text
pyautogui.write('Wake up, Neo', 0.5)
