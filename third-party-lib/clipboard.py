import pyperclip

text = 'some data'

# after running this script, the text will be copied into the clipboard (cross-platform)
pyperclip.copy(text)

# use pyperclip.paste() to get clipboard contents
