import requests


response = requests.get('https://en.wikipedia.org/wiki/Python_(programming_language)')
assert response.status_code == requests.codes.ok  # can throw an exception with response.raise_for_status()

contents = response.text
print(contents[:150])

# write the entire response at once
with open('page.html', 'wb') as f:
    f.write(contents.encode('utf-8'))

# write the response chunk by chunk
with open('page2.html', 'wb') as f:
    for chunk in response.iter_content(10_000):  # bytes per chunk
        f.write(chunk)
