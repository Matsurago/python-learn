from pathlib import Path
import openpyxl
from openpyxl.worksheet.worksheet import Worksheet


# create a new workbook
wb = openpyxl.Workbook()
first_sheet_name = wb.sheetnames[0]
first_sheet: Worksheet = wb[first_sheet_name]  # can also get it with:  wb.active

# set cell value
first_sheet['A1'] = 'New Entry'
first_sheet['B1'] = 79.382
first_sheet['C1'] = '=ROUND($B1, 2)'

# set column width
first_sheet.column_dimensions['C'].width = 20  # default is 8.43
# set row height
first_sheet.row_dimensions[2].height = 30  # default is 12.75

# merge cells
first_sheet.merge_cells('A3:C3')  # use unmerge_cells() to undo

# freeze rows/columns (useful for to keep table headers when scrolling)
first_sheet.freeze_panes = 'A2'  # row 1 (first row in the sheet) will be frozen
# use 'B1' to freeze column A (first column in the sheet)
# set to None to unfreeze

# save
wb.save(Path('./data/new_sheet.xlsx'))
