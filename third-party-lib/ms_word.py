from pathlib import Path
import docx
from docx.document import Document


# read text from DOCX
doc: Document = docx.Document(Path('./data/document.docx'))

for paragraph in doc.paragraphs:
    print(paragraph.text)
